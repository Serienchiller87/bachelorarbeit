<?php
declare(strict_types=1);

namespace Domain\Pages;

class PagesGateway 
{
    protected \PDO $db;

    public function __construct(\PDO $db) {
        $this->db = $db;
    }

    public function selectByName(string $aName): array {
        $stmt = $this->db->prepare('SELECT * FROM tblpages WHERE PageName=:name'); 
        $stmt->execute(['name' => $aName]); 
        return $stmt->fetch();
    }

    public function update(string $pageName, string $pageTitle, string $description): bool {
        $stmt = $this->db->prepare('UPDATE tblpages SET 
                                    PageTitle=:PageTitle, Description=:Description
                                    WHERE PageName=:PageName');
        return $stmt->execute(['PageTitle' => $pageTitle, 'Description' => $description, 'PageName' => $pageName]);
    }
}

?>
