<?php
declare(strict_types=1);

namespace Domain\Pages;

class PageTransactions 
{
    protected PagesGateway $gateway;

    public function __construct(PagesGateway $gateway) {
        $this->gateway = $gateway;
    }

    public function getData(string $name): array {
        return $this->gateway->selectByName($name);
    }

    public function updateData(string $name, string $title, string $description): bool {
        return $this->gateway->update($name, $title, $description);
    }
}

?>
