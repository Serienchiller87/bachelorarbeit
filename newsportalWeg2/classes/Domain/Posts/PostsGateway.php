<?php
declare(strict_types=1);

namespace Domain\Posts;

class PostsGateway 
{
    protected \PDO $db;

    public function __construct(\PDO $db) {
        $this->db = $db;
    }

    public function selectById(int $aId): array {
        $stmt = $this->db->prepare('SELECT tblposts.*, tblcategory.CategoryName, tblsubcategory.SubCategory FROM tblposts 
                                    LEFT JOIN tblcategory ON tblcategory.id=tblposts.CategoryId
                                    LEFT JOIN tblsubcategory ON tblsubcategory.id=tblposts.SubCategoryId
                                    WHERE tblposts.id=:id'); 
        $stmt->execute(['id' => $aId]);
        return $stmt->fetch();
    }

    public function selectActivePostCount(): int {
        $stmt = $this->db->prepare('SELECT COUNT(*) FROM tblposts WHERE Is_Active=1'); 
        $stmt->execute(); 
        return $stmt->fetchColumn();
    }

    public function selectInactivePostCount(): int {
        $stmt = $this->db->prepare('SELECT COUNT(*) FROM tblposts WHERE Is_Active=0'); 
        $stmt->execute(); 
        return $stmt->fetchColumn();
    }

    public function selectActivePostCountStr(string $aStr): int {
        $stmt = $this->db->prepare('SELECT COUNT(*) FROM tblposts WHERE Is_Active=1 AND PostTitle like :str'); 
        $stmt->execute(['str' => '%' . $aStr . '%']); 
        return $stmt->fetchColumn();
    }

    public function selectActivePostCountCat(int $aCatId): int {
        $stmt = $this->db->prepare('SELECT COUNT(*) FROM tblposts WHERE Is_Active=1 AND CategoryId=:catId'); 
        $stmt->execute(['catId' => $aCatId]); 
        return $stmt->fetchColumn();
    }

    public function selectActivePosts(int $aOffset = 0): array { // mit Offset (nur 8 Records)
        $stmt = $this->db->prepare('SELECT tblposts.*, tblcategory.CategoryName FROM tblposts 
                                    LEFT JOIN tblcategory ON tblcategory.id=tblposts.CategoryId 
                                    WHERE tblposts.Is_Active=1 
                                    ORDER BY tblposts.id DESC LIMIT :offset, :recordsPerPage');
        $stmt->execute(['offset' => $aOffset, 'recordsPerPage' => RECORDSPERPAGE]);
        return $stmt->fetchAll();
    }

    public function selectActivePostsStr(string $aStr, int $aOffset = 0): array { // mit Offset (nur 8 Records)
        $stmt = $this->db->prepare('SELECT tblposts.*, tblcategory.CategoryName FROM tblposts 
                                    LEFT JOIN tblcategory ON tblcategory.id=tblposts.CategoryId
                                    WHERE tblposts.PostTitle LIKE :str AND tblposts.Is_Active=1 
                                    ORDER BY tblposts.id DESC LIMIT :offset, :recordsPerPage');
        $stmt->execute(['str' => '%' . $aStr . '%', 'offset' => $aOffset, 'recordsPerPage' => RECORDSPERPAGE]);
        return $stmt->fetchAll();
    }

    public function selectActivePostsCat(int $aCatId, int $aOffset = 0): array { // mit Offset (nur 8 Records)
        $stmt = $this->db->prepare('SELECT tblposts.*, tblcategory.CategoryName FROM tblposts 
                                    LEFT JOIN tblcategory ON tblcategory.id=tblposts.CategoryId 
                                    WHERE tblposts.CategoryId=:catId AND tblposts.Is_Active=1 
                                    ORDER BY tblposts.id DESC LIMIT :offset, :recordsPerPage');
        $stmt->execute(['catId' => $aCatId, 'offset' => $aOffset, 'recordsPerPage' => RECORDSPERPAGE]);
        return $stmt->fetchAll();
    }

    public function insert(string $title, int $catId, int $subcatId, string $details, string $url, bool $isActive, string $img): bool {
        $stmt = $this->db->prepare('INSERT INTO tblposts 
                                    (PostTitle, CategoryId, SubCategoryId, PostDetails, PostUrl, Is_Active, PostImage) 
                                    VALUES 
                                    (:PostTitle, :CategoryId, :SubCategoryId, :PostDetails, :PostUrl, :Is_Active, :PostImage)');
        return $stmt->execute(['PostTitle' => $title, 'CategoryId' => $catId, 'SubCategoryId' => $subcatId, 'PostDetails' => $details, 
                               'PostUrl' => $url, 'Is_Active' => intval($isActive), 'PostImage' => $img]);
    }

    public function update(int $id, string $title, int $catId, int $subcatId, string $details, string $url, bool $isActive): bool {
        $stmt = $this->db->prepare('UPDATE tblposts SET PostTitle=:title, CategoryId=:catId, SubCategoryId=:subcatId, 
                                    PostDetails=:details, PostUrl=:url, Is_Active=:isActive WHERE id=:id');
        return $stmt->execute(['title' => $title, 'catId' => $catId, 'subcatId' => $subcatId, 'details' => $details, 'url' => $url, 'isActive' => intval($isActive), 'id' => $id]);
    }

    public function updateImg(int $id, string $fileName): bool {
        $stmt = $this->db->prepare('UPDATE tblposts SET PostImage=:fileName WHERE id=:id');
        return $stmt->execute(['fileName' => $fileName, 'id' => $id]);
    }

    public function setActiveState(int $id, bool $state): bool {
        $stmt = $this->db->prepare('UPDATE tblposts SET Is_Active=:state WHERE id=:id');
        return $stmt->execute(['id' => $id, 'state' => intval($state)]);
    }

    public function selectByActiveState(bool $isActive): array {
        $stmt = $this->db->prepare('SELECT tblposts.*, tblcategory.CategoryName, tblsubcategory.SubCategory FROM tblposts 
                                    LEFT JOIN tblcategory ON tblcategory.id=tblposts.CategoryId 
                                    LEFT JOIN tblsubcategory ON tblsubcategory.id=tblposts.SubCategoryId
                                    WHERE tblposts.Is_Active=:state ORDER BY tblposts.id DESC');
        $stmt->execute(['state' => intval($isActive)]);
        return $stmt->fetchAll();
    }

    public function delete(int $id): bool {
        $stmt = $this->db->prepare('DELETE FROM tblposts WHERE id=:id');
        return $stmt->execute(['id' => $id]);
    }
}

?>
