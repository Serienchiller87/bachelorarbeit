<?php
declare(strict_types=1);

namespace Domain\Posts;

class PostTransactions 
{
    protected PostsGateway $gateway;
    protected $offset = 0;

    public function __construct(PostsGateway $gateway, int $pageNo = 1) {
        $this->gateway = $gateway;
        $this->offset = ($pageNo - 1) * RECORDSPERPAGE;
    }

    public function getTotalPages(): int {
        return intval(ceil($this->gateway->selectActivePostCount() / RECORDSPERPAGE));
    }

    public function getTotalPagesCat(int $catId): int {
        return intval(ceil($this->gateway->selectActivePostCountCat($catId) / RECORDSPERPAGE));
    }

    public function getTotalPagesStr(string $str): int {
        return intval(ceil($this->gateway->selectActivePostCountStr($str) / RECORDSPERPAGE));
    }

    public function indexPosts(): array {
        return $this->gateway->selectActivePosts($this->offset);
    }

    public function showByActiveState(bool $state): array {
        return $this->gateway->selectByActiveState($state);
    }

    public function indexPostsSidebar(): array {
        return $this->gateway->selectActivePosts(0); // kein Offset, aber nur 8 Stück
    }

    public function indexPostsCat(int $catId): array {
        return $this->gateway->selectActivePostsCat($catId, $this->offset);
    }

    public function indexPostsStr(string $str): array {
        return $this->gateway->selectActivePostsStr($str, $this->offset);
    }

    public function showPost(int $id): array {
        return $this->gateway->selectById($id);
    }

    public function insertPost(string $title, string $imgFile, string $tmpFile, int $catId, int $subcatId, string $description): bool {
        $arr = explode(" ", $title);
        $url = implode("-", $arr);
        $imgNewFile = $this->uploadImg($imgFile, $tmpFile);
        return $this->gateway->insert($title, $catId, $subcatId, $description, $url, true, $imgNewFile);
    }

    public function updatePost(int $id, string $title, int $catId, int $subcatId, string $description): bool {
        $arr = explode(" ", $title);
        $url = implode("-", $arr);
        return $this->gateway->update($id, $title, $catId, $subcatId, $description, $url, true);
    }

    public function changeImg(int $id, string $imgFile, string $tmpFile): bool {
        $imgNewFile = $this->uploadImg($imgFile, $tmpFile);
        return $this->gateway->updateImg($id, $imgNewFile);
    }

    public function setActive(int $id, bool $state): bool {
        return $this->gateway->setActiveState($id, $state);
    }

    public function deleteForever(int $id): bool {
        return $this->gateway->delete($id);
    }

    protected function uploadImg(string $imgFile, string $tmpFile): string {
        $result = '';
        // get the image extension
        $extension = substr($imgFile, strlen($imgFile) - 4, strlen($imgFile));
        // allowed extensions
        $allowedExtensions = array(".jpg", "jpeg", ".png", ".gif");
        // Validation for allowed extensions .in_array() function searches an array for a specific value.
        if(in_array($extension, $allowedExtensions)) {
            //rename the image file
            $result = md5($imgFile) . $extension;
            // Code for move image into directory
            move_uploaded_file($tmpFile, "admin/postimages/" . $result);
        }
        return $result;
    }
}

?>
