<?php
declare(strict_types=1);

namespace Domain\Comments;

class CommentTransactions
{
    protected CommentsGateway $gateway;

    public function __construct(CommentsGateway $gateway) {
        $this->gateway = $gateway;

        //Genrating CSRF Token
        if(empty($_SESSION['token'])) {
            $_SESSION['token'] = bin2hex(random_bytes(32));
        }
    }

    public function insertData(int $postId, string $name, string $email, string $comment, string $csrfToken): bool {
        $result = false;
        //Verifying CSRF Token
        if(!empty($csrfToken) && hash_equals($_SESSION['token'], $csrfToken)) {
            $query = $this->gateway->insert($postId, $name, $email, $comment, false); // vorerst inaktiv
            if($query) {
                unset($_SESSION['token']);
                $result = true;
            }
        }
        return $result;
    }

    public function showActiveComments(int $postId): array {
        return $this->gateway->selectActiveByPostId($postId);
    }

    public function showAllActive(): array {
        return $this->gateway->selectAllActive();
    }

    public function showAllInactive(): array {
        return $this->gateway->selectAllInactive();
    }

    public function setActive(int $id, bool $state): bool {
        return $this->gateway->setActiveState($id, $state);
    }

    public function deleteForever(int $id): bool {
        return $this->gateway->delete($id);
    }
}

?>
