<?php
declare(strict_types=1);

namespace Domain\Comments;

class CommentsGateway 
{
    protected \PDO $db;

    public function __construct(\PDO $db) {
        $this->db = $db;
    }

    public function selectActiveByPostId(int $aId): array {
        $stmt = $this->db->prepare('SELECT * FROM tblcomments WHERE postId=:id AND status=1'); 
        $stmt->execute(['id' => $aId]); 
        return $stmt->fetchAll();
    }

    public function selectAllActive(): array { // nur aktive für die ein Post existiert
        $stmt = $this->db->prepare('SELECT tblcomments.*, tblposts.PostTitle FROM tblcomments
                                    RIGHT JOIN tblposts ON tblposts.id=tblcomments.postId
                                    WHERE tblcomments.status=1'); 
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function selectAllInactive(): array { // nur inaktive für die ein Post existiert
        $stmt = $this->db->prepare('SELECT tblcomments.*, tblposts.PostTitle FROM tblcomments
                                    RIGHT JOIN tblposts ON tblposts.id=tblcomments.postId
                                    WHERE tblcomments.status=0'); 
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function insert(int $postId, string $name, string $email, string $comment, bool $isActive): bool {
        $stmt = $this->db->prepare('INSERT INTO tblcomments (postId, name, email, comment, status) 
                                    VALUES (:postId, :name, :email, :comment, :status)');
        return $stmt->execute(['postId' => $postId, 'name' => $name, 'email' => $email, 'comment' => $comment, 'status' => intval($isActive)]);
    }

    public function setActiveState(int $id, bool $state): bool {
        $stmt = $this->db->prepare('UPDATE tblcomments SET status=:state WHERE id=:id');
        return $stmt->execute(['id' => $id, 'state' => intval($state)]);
    }

    public function delete(int $id): bool {
        $stmt = $this->db->prepare('DELETE FROM tblcomments WHERE id=:id');
        return $stmt->execute(['id' => $id]);
    }
}

?>
