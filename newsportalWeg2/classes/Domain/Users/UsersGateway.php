<?php
declare(strict_types=1);

namespace Domain\Users;

class UsersGateway 
{
    protected \PDO $db;

    public function __construct(\PDO $db) {
        $this->db = $db;
    }

    public function selectByName(string $aName): array {
        $stmt = $this->db->prepare('SELECT * FROM tbladmin WHERE AdminUserName=:name'); 
        $stmt->execute(['name' => $aName]);
        $result = $stmt->fetch();
        if($result) return $result; else return array();
    }

    public function update(string $newPW, string $name): bool {
        $stmt = $this->db->prepare('UPDATE tbladmin SET AdminPassword=:newPW where AdminUserName=:name');
        return $stmt->execute(['newPW' => $newPW, 'name' => $name]);
    }
}

?>
