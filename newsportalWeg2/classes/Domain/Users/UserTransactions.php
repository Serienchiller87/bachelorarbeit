<?php
declare(strict_types=1);

namespace Domain\Users;

class UserTransactions 
{
    protected UsersGateway $gateway;

    public function __construct(UsersGateway $gateway) {
        $this->gateway = $gateway;
    }

    public function login(string $name, string $password): bool {
        $result = false;
        $user = $this->gateway->selectByName($name);
        if(!empty($user)) {
            // verifying Password
            if (password_verify($password, $user['AdminPassword'])) {
                $_SESSION['login'] = $name;
                $result = true;
            }
        }
        return $result;
    }

    public function changePassword(string $old, string $new): bool {
        $result = false;
        //Current Password hashing 
        $options = ['cost' => 12];
        $hashedpass = password_hash($old, PASSWORD_BCRYPT, $options);
        // new password hashing 
        $newhashedpass = password_hash($new, PASSWORD_BCRYPT, $options);
        $user = $this->gateway->selectByName($_SESSION['login']);
        if(password_verify($old, $user['AdminPassword'])) {
            $result = $this->gateway->update($newhashedpass, $_SESSION['login']);
        }
        return $result;
    }
}

?>
