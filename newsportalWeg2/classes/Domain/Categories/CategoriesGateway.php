<?php
declare(strict_types=1);

namespace Domain\Categories;

class CategoriesGateway 
{
    protected \PDO $db;

    public function __construct(\PDO $db) {
        $this->db = $db;
    }

    public function selectById(int $id): array {
        $stmt = $this->db->prepare('SELECT * FROM tblcategory WHERE id=:id'); 
        $stmt->execute(['id' => $id]);
        return $stmt->fetch();
    }

    public function selectAllActive(): array {
        $stmt = $this->db->prepare('SELECT * FROM tblcategory WHERE Is_Active=1 ORDER BY CategoryName'); 
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function selectAllInactive(): array {
        $stmt = $this->db->prepare('SELECT * FROM tblcategory WHERE Is_Active=0 ORDER BY CategoryName'); 
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function insert(string $name, string $description, bool $isActive): bool {
        $stmt = $this->db->prepare('INSERT INTO tblcategory (CategoryName, Description, Is_Active) 
                                    VALUES (:CategoryName, :Description, :Is_Active)');
        return $stmt->execute(['CategoryName' => $name, 'Description' => $description, 'Is_Active' => intval($isActive)]);
    }

    public function update(int $id, string $name, string $description): bool {
        $stmt = $this->db->prepare('UPDATE tblcategory SET CategoryName=:CategoryName, Description=:Description 
                                    WHERE id=:id');
        return $stmt->execute(['CategoryName' => $name, 'Description' => $description, 'id' => $id]);
    }

    public function setActiveState(int $id, bool $state): bool {
        $stmt = $this->db->prepare('UPDATE tblcategory SET Is_Active=:state WHERE id=:id');
        return $stmt->execute(['id' => $id, 'state' => intval($state)]);
    }

    public function delete(int $id): bool {
        $stmt = $this->db->prepare('DELETE FROM tblcategory WHERE id=:id');
        return $stmt->execute(['id' => $id]);
    }
}

?>
