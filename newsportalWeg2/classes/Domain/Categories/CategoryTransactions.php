<?php
declare(strict_types=1);

namespace Domain\Categories;

class CategoryTransactions
{
    protected CategoriesGateway $gateway;

    public function __construct(CategoriesGateway $gateway) {
        $this->gateway = $gateway;
    }

    public function showActiveCats(): array {
        return $this->gateway->selectAllActive();
    }

    public function showInactiveCats(): array {
        return $this->gateway->selectAllInactive();
    }

    public function showCat(int $id): array {
        return $this->gateway->selectById($id);
    }

    public function insertData(string $name, string $description): bool {
        return $this->gateway->insert($name, $description, true);
    }

    public function updateData(int $id, string $name, string $description): bool {
        return $this->gateway->update($id, $name, $description);
    }

    public function setActive(int $id, bool $state): bool {
        return $this->gateway->setActiveState($id, $state);
    }

    public function deleteForever(int $id): bool {
        return $this->gateway->delete($id);
    }
}

?>
