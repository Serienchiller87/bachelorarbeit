<?php
declare(strict_types=1);

namespace Domain\Subcategories;

class SubcategoriesGateway 
{
    protected \PDO $db;

    public function __construct(\PDO $db) {
        $this->db = $db;
    }

    public function selectById(int $id): array {
        $stmt = $this->db->prepare('SELECT tblsubcategory.*, tblcategory.CategoryName FROM tblsubcategory 
                                    LEFT JOIN tblcategory ON tblsubcategory.CategoryId=tblcategory.id
                                    WHERE tblsubcategory.id=:id'); 
        $stmt->execute(['id' => $id]);
        return $stmt->fetch();
    }

    public function selectAllActive(): array {
        $stmt = $this->db->prepare('SELECT tblsubcategory.*, tblcategory.CategoryName FROM tblsubcategory 
                                    JOIN tblcategory ON tblsubcategory.CategoryId=tblcategory.id
                                    WHERE tblsubcategory.Is_Active=1');
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function selectAllInactive(): array {
        $stmt = $this->db->prepare('SELECT tblsubcategory.*, tblcategory.CategoryName FROM tblsubcategory 
                                    JOIN tblcategory ON tblsubcategory.CategoryId=tblcategory.id
                                    WHERE tblsubcategory.Is_Active=0');
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function selectAllActiveByCat(int $catId): array {
        $stmt = $this->db->prepare('SELECT * FROM tblsubcategory WHERE CategoryId=:catId AND Is_Active=1'); 
        $stmt->execute(['catId' => $catId]);
        return $stmt->fetchAll();
    }

    public function insert(int $catId, string $name, string $description, bool $isActive): bool {
        $stmt = $this->db->prepare('INSERT INTO tblsubcategory (CategoryId, Subcategory, SubCatDescription, Is_Active) 
                                    VALUES (:catId, :name, :description, :Is_Active)');
        return $stmt->execute(['catId' => $catId, 'name' => $name, 'description' => $description, 'Is_Active' => intval($isActive)]);
    }

    public function update(int $id, int $catId, string $name, string $description): bool {
        $stmt = $this->db->prepare('UPDATE tblsubcategory SET 
                                    CategoryId=:catId, Subcategory=:name, SubCatDescription=:description 
                                    WHERE id=:id');
        return $stmt->execute(['catId' => $catId, 'name' => $name, 'description' => $description, 'id' => $id]);
    }

    public function setActiveState(int $id, bool $state): bool {
        $stmt = $this->db->prepare('UPDATE tblsubcategory SET Is_Active=:state WHERE id=:id');
        return $stmt->execute(['id' => $id, 'state' => intval($state)]);
    }

    public function delete(int $id): bool {
        $stmt = $this->db->prepare('DELETE FROM tblsubcategory WHERE id=:id');
        return $stmt->execute(['id' => $id]);
    }
}

?>
