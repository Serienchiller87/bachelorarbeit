<?php
declare(strict_types=1);

namespace Domain\Subcategories;

class SubcategoryTransactions
{
    protected SubcategoriesGateway $gateway;

    public function __construct(SubcategoriesGateway $gateway) {
        $this->gateway = $gateway;
    }

    public function insertData(int $catId, string $name, string $description): bool {
        return $this->gateway->insert($catId, $name, $description, true);
    }

    public function updateData(int $id, int $catId, string $name, string $description): bool {
        return $this->gateway->update($id, $catId, $name, $description);
    }

    public function showAllActive(): array {
        return $this->gateway->selectAllActive();
    }

    public function showAllInactive(): array {
        return $this->gateway->selectAllInactive();
    }

    public function showAllActiveByCat(int $catId): array {
        return $this->gateway->selectAllActiveByCat($catId);
    }

    public function showSubcat(int $id): array {
        return $this->gateway->selectById($id);
    }

    public function setActive(int $id, bool $state): bool {
        return $this->gateway->setActiveState($id, $state);
    }

    public function deleteForever(int $id): bool {
        return $this->gateway->delete($id);
    }
}

?>
