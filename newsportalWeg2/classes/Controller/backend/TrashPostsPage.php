<?php
declare(strict_types=1);

namespace Controller\backend;
use \Mlaphp\Response;
use \Domain\Posts\PostTransactions;

class TrashPostsPage
{
    protected PostTransactions $postTransactions;
    protected Response $response;

    public function __construct(PostTransactions $postTransactions, Response $response) {
        $this->postTransactions = $postTransactions;
        $this->response = $response;
    }

    public function run(): Response {
        // Code for restore
        if(($_GET['action'] ?? '') == 'restore') {
            if($this->postTransactions->setActive(intval($_GET['pid']), true)) {
                $msg = 'Post was restored successfully.';
            } else {
                $msg = ERRORMESSAGE;
            }
        }

        // Code for forever delete
        if($_GET['presid'] ?? '') {
            if($this->postTransactions->deleteForever(intval($_GET['presid']))) {
                $msg = 'Post was deleted forever.';
            } else {
                $msg = ERRORMESSAGE;
            }
        }

        // Daten holen
        $posts = $this->postTransactions->showByActiveState(false);

        // Präsentation
        $this->response->setView('backend/postIndex.html.php');
        $this->response->setVars(array(
            'title' => 'Trashed posts',
            'posts' => $posts,
            'showActive' => false,
            'msg' => $msg ?? ''
        ));
        return $this->response;
    }
}

?>
