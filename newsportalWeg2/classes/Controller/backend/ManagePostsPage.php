<?php
declare(strict_types=1);

namespace Controller\backend;
use \Mlaphp\Response;
use \Domain\Posts\PostTransactions;

class ManagePostsPage
{
    protected PostTransactions $postTransactions;
    protected Response $response;

    public function __construct(PostTransactions $postTransactions, Response $response) {
        $this->postTransactions = $postTransactions;
        $this->response = $response;
    }

    public function run(): Response {
        // Code for set inactive
        if(($_GET['action'] ?? '') == 'del') {
            if($this->postTransactions->setActive(intval($_GET['pid']), false)) {
                $msg = 'Post was deleted successfully.';
            } else {
                $msg = ERRORMESSAGE;
            }
        }

        // Daten holen
        $posts = $this->postTransactions->showByActiveState(true);

        // Präsentation
        $this->response->setView('backend/postIndex.html.php');
        $this->response->setVars(array(
            'title' => 'Manage posts',
            'posts' => $posts,
            'showActive' => true,
            'msg' => $msg ?? ''
        ));
        return $this->response;
    }
}

?>
