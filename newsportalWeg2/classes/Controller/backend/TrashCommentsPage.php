<?php
declare(strict_types=1);

namespace Controller\backend;
use \Mlaphp\Response;
use \Domain\Comments\CommentTransactions;

class TrashCommentsPage
{
    protected CommentTransactions $commentTransactions;
    protected Response $response;

    public function __construct(CommentTransactions $commentTransactions, Response $response) {
        $this->commentTransactions = $commentTransactions;
        $this->response = $response;
    }

    public function run(): Response {
        // Code for restore
        if($_GET['appid'] ?? '') {
            if($this->commentTransactions->setActive(intval($_GET['appid']), true)) {
                $msg = 'Comment was approved successfully.';
            } else {
                $msg = ERRORMESSAGE;
            }
        }

        // Code for delete forever
        if(($_GET['action'] ?? '') == 'del' && $_GET['rid']) {
            if($this->commentTransactions->deleteForever(intval($_GET['rid']))) {
                $msg = 'Comment was deleted forever.';
            } else {
                $msg = ERRORMESSAGE;
            }
        }

        // Daten holen
        $comments = $this->commentTransactions->showAllInactive();

        // Präsentation
        $this->response->setView('backend/comments.html.php');
        $this->response->setVars(array(
            'title' => 'Unapprove comments',
            'comments' => $comments,
            'showActive' => false,
            'msg' => $msg ?? ''
        ));
        return $this->response;
    }
}

?>
