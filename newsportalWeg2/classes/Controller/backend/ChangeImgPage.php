<?php
declare(strict_types=1);

namespace Controller\backend;
use \Mlaphp\Response;
use \Domain\Posts\PostTransactions;

class ChangeImgPage
{
    protected PostTransactions $postTransactions;
    protected Response $response;

    public function __construct(PostTransactions $postTransactions, Response $response) {
        $this->postTransactions = $postTransactions;
        $this->response = $response;
    }

    public function run(): Response {
        // Daten speichern
        if(isset($_POST['update'])) {
            if($this->postTransactions->changeImg(intval($_GET['nid']), $_FILES["postimage"]["name"], $_FILES["postimage"]["tmp_name"])) {
                $msg = 'Post image was updated successfully.';
            } else {
                $msg = ERRORMESSAGE;
            }
        }

        // Daten holen
        $post = $this->postTransactions->showPost(intval($_GET['nid']));

        // Präsentation
        $this->response->setView('backend/image.html.php');
        $this->response->setVars(array(
            'title' => 'Update post image',
            'post' => $post,
            'msg' => $msg ?? ''
        ));
        return $this->response;
    }
}

?>
