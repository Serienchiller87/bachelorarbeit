<?php
declare(strict_types=1);

namespace Controller\backend;
use \Mlaphp\Response;
use \Domain\Categories\CategoryTransactions;

class ManageCatsPage
{
    protected CategoryTransactions $catTransactions;
    protected Response $response;

    public function __construct(CategoryTransactions $catTransactions, Response $response) {
        $this->catTransactions = $catTransactions;
        $this->response = $response;
    }

    public function run(): Response {
        // Code for set inactive
        if(($_GET['action'] ?? '') == 'del' && $_GET['rid']) { 
            if($this->catTransactions->setActive(intval($_GET['rid']), false)) {
                $msg = 'Category was deleted successfully.';
            } else {
                $msg = ERRORMESSAGE;
            }
        } 

        // Code for restore
        if($_GET['resid'] ?? '') { 
            if($this->catTransactions->setActive(intval($_GET['resid']), true)) {
                $msg = 'Category was restored successfully.';
            } else {
                $msg = ERRORMESSAGE;
            }
        } 

        // Code for forever delete
        if(($_GET['action'] ?? '') == 'parmdel' && $_GET['rid']) { 
            if($this->catTransactions->deleteForever(intval($_GET['rid']))) {
                $msg = 'Category was deleted forever.';
            } else {
                $msg = ERRORMESSAGE;
            }
        }

        // Daten holen
        $activeCats = $this->catTransactions->showActiveCats();
        $inactiveCats = $this->catTransactions->showInactiveCats();

        // Präsentation
        $this->response->setView('backend/catIndex.html.php');
        $this->response->setVars(array(
            'title' => 'Manage categories',
            'activeCats' => $activeCats,
            'inactiveCats' => $inactiveCats,
            'msg' => $msg ?? ''
        ));
        return $this->response;
    }
}

?>
