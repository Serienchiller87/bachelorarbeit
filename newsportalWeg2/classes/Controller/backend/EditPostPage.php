<?php
declare(strict_types=1);

namespace Controller\backend;
use \Mlaphp\Response;
use \Domain\Posts\PostTransactions;
use \Domain\Categories\CategoryTransactions;

class EditPostPage
{
    protected PostTransactions $postTransactions;
    protected CategoryTransactions $catTransactions;
    protected Response $response;

    public function __construct(PostTransactions $postTransactions, CategoryTransactions $catTransactions, Response $response) {
        $this->postTransactions = $postTransactions;
        $this->catTransactions = $catTransactions;
        $this->response = $response;
    }

    public function run(): Response {
        // Daten speichern
        if(isset($_POST['submit'])) {
            if($this->postTransactions->updatePost(intval($_GET['nid']), $_POST['posttitle'], intval($_POST['category']), intval($_POST['subcategory']), $_POST['postdescription'])) {
                $msg = 'Post was updated successfully.';
            } else {
                $msg = ERRORMESSAGE;
            }
        }

        // Daten holen
        $post = $this->postTransactions->showPost(intval($_GET['nid']));
        $cats = $this->catTransactions->showActiveCats();

        // Präsentation
        $this->response->setView('backend/postEdit.html.php');
        $this->response->setVars(array(
            'title' => 'Edit post',
            'post' => $post,
            'cats' => $cats,
            'msg' => $msg ?? ''
        ));
        return $this->response;
    }
}

?>
