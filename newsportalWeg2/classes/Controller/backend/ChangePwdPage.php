<?php
declare(strict_types=1);

namespace Controller\backend;
use \Mlaphp\Response;
use \Domain\Users\UserTransactions;

class ChangePwdPage 
{
    protected UserTransactions $userTransactions;
    protected Response $response;

    public function __construct(UserTransactions $userTransactions, Response $response) {
        $this->userTransactions = $userTransactions;
        $this->response = $response;
    }

    public function run(): Response {
        // Daten speichern
        if(isset($_POST['submit'])) {
            if($this->userTransactions->changePassword($_POST['password'], $_POST['newpassword'])) {
                $msg = 'Password was changed successfully.';
            } else {
                $msg = 'Error: Old password does not match.';
            }
        }

        // Präsentation
        $this->response->setView('backend/password.html.php');
        $this->response->setVars(array(
            'title' => 'Change password',
            'msg' => $msg ?? ''
        ));
        return $this->response;
    }
}

?>
