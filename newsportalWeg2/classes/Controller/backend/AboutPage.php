<?php
declare(strict_types=1);

namespace Controller\backend;
use \Mlaphp\Response;
use \Domain\Pages\PageTransactions;

class AboutPage 
{
    protected PageTransactions $pageTransactions;
    protected Response $response;

    public function __construct(PageTransactions $pageTransactions, Response $response) {
        $this->pageTransactions = $pageTransactions;
        $this->response = $response;
    }

    public function run(): Response {
        // Daten speichern
        if(isset($_POST['update'])) {
            if($this->pageTransactions->updateData('aboutus', $_POST['pagetitle'], $_POST['pagedescription'])) {
                $msg = 'About page was updated successfully.';
            } else {
                $msg = ERRORMESSAGE;
            }
        }

        // Daten holen
        $page = $this->pageTransactions->getData('aboutus');

        // Präsentation
        $this->response->setView('backend/info.html.php');
        $this->response->setVars(array(
            'title' => 'About',
            'page' => $page,
            'msg' => $msg ?? ''
        ));
        return $this->response;
    }
}

?>
