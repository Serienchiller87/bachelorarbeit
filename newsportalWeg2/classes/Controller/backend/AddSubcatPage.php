<?php
declare(strict_types=1);

namespace Controller\backend;
use Mlaphp\Response;
use Domain\Subcategories\SubcategoryTransactions;
use Domain\Categories\CategoryTransactions;

class AddSubcatPage
{
    protected SubcategoryTransactions $subcatTransactions;
    protected CategoryTransactions $catTransactions;
    protected Response $response;

    public function __construct(SubcategoryTransactions $subcatTransactions, CategoryTransactions $catTransactions, Response $response) {
        $this->subcatTransactions = $subcatTransactions;
        $this->catTransactions = $catTransactions;
        $this->response = $response;
    }

    public function run(): Response {
        // Daten speichern
        if(isset($_POST['submitsubcat'])) {
            if($this->subcatTransactions->insertData(intval($_POST['category']), $_POST['subcategory'], $_POST['sucatdescription'])) {
                $msg = 'Subcategory created successfully.';
            } else {
                $msg = ERRORMESSAGE;
            }
        }

        // Daten holen 
        $cats = $this->catTransactions->showActiveCats();

        // Präsentation
        $this->response->setView('backend/subcatEdit.html.php');
        $this->response->setVars(array(
            'title' => 'Add subcategory',
            'cats' => $cats,
            'msg' => $msg ?? ''
        ));
        return $this->response;
    }
}

?>
