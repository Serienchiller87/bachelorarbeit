<?php
declare(strict_types=1);

namespace Controller\backend;
use \Mlaphp\Response;
use \Domain\Categories\CategoryTransactions;

class AddCatPage
{
    protected CategoryTransactions $catTransactions;
    protected Response $response;

    public function __construct(CategoryTransactions $catTransactions, Response $response) {
        $this->catTransactions = $catTransactions;
        $this->response = $response;
    }

    public function run(): Response {
        // Daten speichern
        if(isset($_POST['submit'])) {
            if($this->catTransactions->insertData($_POST['category'], $_POST['description'])) {
                $msg = 'Category created successfully.';
            } else {
                $msg = ERRORMESSAGE;
            }
        }

        // Präsentation
        $this->response->setView('backend/catEdit.html.php');
        $this->response->setVars(array(
            'title' => 'Add category',
            'msg' => $msg ?? ''
        ));
        return $this->response;
    }
}

?>
