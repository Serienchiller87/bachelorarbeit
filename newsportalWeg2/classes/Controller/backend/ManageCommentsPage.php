<?php
declare(strict_types=1);

namespace Controller\backend;
use \Mlaphp\Response;
use \Domain\Comments\CommentTransactions;

class ManageCommentsPage
{
    protected CommentTransactions $commentTransactions;
    protected Response $response;

    public function __construct(CommentTransactions $commentTransactions, Response $response) {
        $this->commentTransactions = $commentTransactions;
        $this->response = $response;
    }

    public function run(): Response {
        // Code for set inactive
        if($_GET['disid'] ?? '') {
            if($this->commentTransactions->setActive(intval($_GET['disid']), false)) {
                $msg = 'Comment was unapproved successfully.';
            } else {
                $msg = ERRORMESSAGE;
            }
        }

        // Daten holen
        $comments = $this->commentTransactions->showAllActive();

        // Präsentation
        $this->response->setView('backend/comments.html.php');
        $this->response->setVars(array(
            'title' => 'Manage comments',
            'comments' => $comments,
            'showActive' => true,
            'msg' => $msg ?? ''
        ));
        return $this->response;
    }
}

?>
