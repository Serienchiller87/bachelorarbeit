<?php
declare(strict_types=1);

namespace Controller\backend;
use \Mlaphp\Response;
use \Domain\Posts\PostTransactions;
use \Domain\Categories\CategoryTransactions;
use \Domain\Subcategories\SubcategoryTransactions;

class DashboardPage 
{
    protected PostTransactions $postTransactions;
    protected CategoryTransactions $catTransactions;
    protected SubcategoryTransactions $subcatTransactions;
    protected Response $response;

    public function __construct(PostTransactions $postTransactions, CategoryTransactions $catTransactions, SubcategoryTransactions $subcatTransactions, Response $response) {
        $this->postTransactions = $postTransactions;
        $this->catTransactions = $catTransactions;
        $this->subcatTransactions = $subcatTransactions;
        $this->response = $response;
    }

    public function run(): Response {
        // Daten holen
        $cats = $this->catTransactions->showActiveCats();
        $subcats = $this->subcatTransactions->showAllActive();
        $activeCount = count($this->postTransactions->showByActiveState(true));
        $inactiveCount = count($this->postTransactions->showByActiveState(false));

        // Präsentation
        $this->response->setView('backend/dashboard.html.php');
        $this->response->setVars(array(
            'title' => 'Dashboard',
            'cats' => $cats,
            'subcats' => $subcats,
            'activeCount' => $activeCount,
            'inactiveCount' => $inactiveCount
        ));
        return $this->response;
    }
}

?>
