<?php
declare(strict_types=1);

namespace Controller\backend;
use \Mlaphp\Response;
use \Domain\Categories\CategoryTransactions;

class EditCatPage
{
    protected CategoryTransactions $catTransactions;
    protected Response $response;

    public function __construct(CategoryTransactions $catTransactions, Response $response) {
        $this->catTransactions = $catTransactions;
        $this->response = $response;
    }

    public function run(): Response {
        // Daten speichern
        if(isset($_POST['submit'])) {
            if($this->catTransactions->updateData(intval($_GET['cid']), $_POST['category'], $_POST['description'])) {
                $msg = 'Category was updated successfully.';
            } else {
                $msg = ERRORMESSAGE;
            }
        }

        // Daten holen
        $cat = $this->catTransactions->showCat(intval($_GET['cid']));

        // Präsentation
        $this->response->setView('backend/catEdit.html.php');
        $this->response->setVars(array(
            'title' => 'Edit category',
            'cat' => $cat,
            'msg' => $msg ?? ''
        ));
        return $this->response;
    }
}

?>
