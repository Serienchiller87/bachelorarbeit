<?php
declare(strict_types=1);

namespace Controller\backend;
use \Mlaphp\Response;
use \Domain\Subcategories\SubcategoryTransactions;

class ManageSubcatsPage
{
    protected SubcategoryTransactions $subcatTransactions;
    protected Response $response;

    public function __construct(SubcategoryTransactions $subcatTransactions, Response $response) {
        $this->subcatTransactions = $subcatTransactions;
        $this->response = $response;
    }

    public function run(): Response {
        // Code for set inactive
        if(($_GET['action'] ?? '') == 'del' && $_GET['scid']) {
            if($this->subcatTransactions->setActive(intval($_GET['scid']), false)) {
                $msg = 'Subcategory was deleted successfully.';
            } else {
                $msg = ERRORMESSAGE;
            }
        }

        // Code for restore
        if($_GET['resid'] ?? '') {
            if($this->subcatTransactions->setActive(intval($_GET['resid']), true)) {
                $msg = 'Subcategory was restored successfully.';
            } else {
                $msg = ERRORMESSAGE;
            }
        }

        // Code for forever delete
        if(($_GET['action'] ?? '') == 'perdel' && $_GET['scid']) {
            if($this->subcatTransactions->deleteForever(intval($_GET['scid']))) {
                $msg = 'Subcategory was deleted forever.';
            } else {
                $msg = ERRORMESSAGE;
            }
        }

        // Daten holen
        $activeSubcats = $this->subcatTransactions->showAllActive();
        $inactiveSubcats = $this->subcatTransactions->showAllInactive();

        // Präsentation
        $this->response->setView('backend/subcatIndex.html.php');
        $this->response->setVars(array(
            'title' => 'Manage subcategories',
            'activeSubcats' => $activeSubcats,
            'inactiveSubcats' => $inactiveSubcats,
            'msg' => $msg ?? ''
        ));
        return $this->response;
    }
}

?>
