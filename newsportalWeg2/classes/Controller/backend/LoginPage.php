<?php
declare(strict_types=1);

namespace Controller\backend;
use \Mlaphp\Response;
use \Domain\Users\UserTransactions;

class LoginPage 
{
    protected UserTransactions $userTransactions;
    protected Response $response;

    public function __construct(UserTransactions $userTransactions, Response $response) {
        $this->userTransactions = $userTransactions;
        $this->response = $response;
    }

    public function run(): Response {
        // Login Vorgang
        if(isset($_POST['login'])) {
            if($this->userTransactions->login($_POST['username'], $_POST['password'])) {
                header('location: dashboard.php');
                exit();
            } else {
                echo '<script>alert("Error: Username or password do not match.");</script>';
            }
        }

        // Präsentation
        $this->response->setView('backend/login.html.php');
        $this->response->setVars(array(
            'title' => 'Admin login',
        ));
        return $this->response;
    }
}

?>
