<?php
declare(strict_types=1);

namespace Controller\frontend;
use \Mlaphp\Response;
use \Domain\Posts\PostTransactions;
use \Domain\Categories\CategoryTransactions;
use \Domain\Comments\CommentTransactions;

class DetailsPage 
{
    protected PostTransactions $postTransactions;
    protected CategoryTransactions $catTransactions;
    protected CommentTransactions $commentTransactions;
    protected Response $response;

    public function __construct(PostTransactions $postTransactions, CategoryTransactions $catTransactions, CommentTransactions $commentTransactions, Response $response) {
        $this->postTransactions = $postTransactions;
        $this->catTransactions = $catTransactions;
        $this->commentTransactions = $commentTransactions;
        $this->response = $response;
    }

    public function run(): Response {
        // Kommentar speichern
        if(isset($_POST['submit'])) {
            if($this->commentTransactions->insertData(intval($_GET['nid']), $_POST['name'], $_POST['email'], $_POST['comment'], $_POST['csrftoken'])) {
                echo '<script>alert("Your comment was successfully submit. It will be displayed after admin review.");</script>';
            } else {
                echo '<script>alert("Something went wrong. Please try again.");</script>';
            }
        }

        // Daten holen
        $post = $this->postTransactions->showPost(intval($_GET['nid']));
        $comments = $this->commentTransactions->showActiveComments(intval($_GET['nid']));
        
        // Sidebar Daten
        $catsSide = $this->catTransactions->showActiveCats();
        $postsSide = $this->postTransactions->indexPostsSidebar();

        // Präsentation
        $this->response->setView('frontend/details.html.php');
        $this->response->setVars(array(
            'title' => 'Details Page',
            'post' => $post,
            'comments' => $comments,
            'catsSide' => $catsSide,
            'postsSide' => $postsSide
        ));
        return $this->response;
    }
}

?>
