<?php
declare(strict_types=1);

namespace Controller\frontend;
use \Mlaphp\Response;
use \Domain\Posts\PostTransactions;
use \Domain\Categories\CategoryTransactions;

class IndexPage 
{
    protected PostTransactions $postTransactions;
    protected CategoryTransactions $catTransactions;
    protected Response $response;

    public function __construct(PostTransactions $postTransactions, CategoryTransactions $catTransactions, Response $response) {
        $this->postTransactions = $postTransactions;
        $this->catTransactions = $catTransactions;
        $this->response = $response;
    }

    public function run(): Response {
        // Daten holen
        $totalPages = $this->postTransactions->getTotalPages();
        $posts = $this->postTransactions->indexPosts();

        // Sidebar Daten
        $catsSide = $this->catTransactions->showActiveCats();
        $postsSide = $this->postTransactions->indexPostsSidebar();

        // Präsentation
        $this->response->setView('frontend/index.html.php');
        $this->response->setVars(array(
            'title' => 'Home Page',
            'pageno' => $_GET['pageno'] ?? 1,
            'totalPages' => $totalPages,
            'posts' => $posts,
            'catsSide' => $catsSide,
            'postsSide' => $postsSide
        ));
        return $this->response;
    }
}

?>
