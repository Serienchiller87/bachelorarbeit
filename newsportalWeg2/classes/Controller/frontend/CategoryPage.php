<?php
declare(strict_types=1);

namespace Controller\frontend;
use \Mlaphp\Response;
use \Domain\Posts\PostTransactions;
use \Domain\Categories\CategoryTransactions;

class CategoryPage 
{
    protected PostTransactions $postTransactions;
    protected CategoryTransactions $catTransactions;
    protected Response $response;

    public function __construct(PostTransactions $postTransactions, CategoryTransactions $catTransactions, Response $response) {
        $this->postTransactions = $postTransactions;
        $this->catTransactions = $catTransactions;
        $this->response = $response;
    }

    public function run(): Response {
        // $_GET in Session übernehmen (für Pagination)
        if(($_GET['catid'] ?? '') != '') {
            $_SESSION['catid'] = intval($_GET['catid']);
        }

        // Daten holen
        $totalPages = $this->postTransactions->getTotalPagesCat($_SESSION['catid']);
        $posts = $this->postTransactions->indexPostsCat($_SESSION['catid']);

        // Sidebar Daten
        $catsSide = $this->catTransactions->showActiveCats();
        $postsSide = $this->postTransactions->indexPostsSidebar();

        // Präsentation
        $this->response->setView('frontend/index.html.php');
        $this->response->setVars(array(
            'title' => 'Categories Page',
            'pageno' => $_GET['pageno'] ?? 1,
            'totalPages' => $totalPages,
            'posts' => $posts,
            'categoryName' => $posts[0]['CategoryName'] ?? '',
            'catsSide' => $catsSide,
            'postsSide' => $postsSide
        ));
        return $this->response;
    }
}

?>
