<?php
declare(strict_types=1);

namespace Controller\frontend;
use \Mlaphp\Response;
use \Domain\Posts\PostTransactions;
use \Domain\Categories\CategoryTransactions;

class SearchPage 
{
    protected PostTransactions $postTransactions;
    protected CategoryTransactions $catTransactions;
    protected Response $response;

    public function __construct(PostTransactions $postTransactions, CategoryTransactions $catTransactions, Response $response) {
        $this->postTransactions = $postTransactions;
        $this->catTransactions = $catTransactions;
        $this->response = $response;
    }

    public function run(): Response {
        // $_GET in Session übernehmen (für Pagination)
        if(($_POST['searchtitle'] ?? '') != '') {
            $_SESSION['searchtitle'] = $_POST['searchtitle'];
        }

        // Daten holen
        $totalPages = $this->postTransactions->getTotalPagesStr($_SESSION['searchtitle']);
        $posts = $this->postTransactions->indexPostsStr($_SESSION['searchtitle']);

        // Sidebar Daten
        $catsSide = $this->catTransactions->showActiveCats();
        $postsSide = $this->postTransactions->indexPostsSidebar();

        // Präsentation
        $this->response->setView('frontend/index.html.php');
        $this->response->setVars(array(
            'title' => 'Search Page',
            'pageno' => $_GET['pageno'] ?? 1,
            'totalPages' => $totalPages,
            'posts' => $posts,
            'catsSide' => $catsSide,
            'postsSide' => $postsSide
        ));
        return $this->response;
    }
}

?>
