<?php
declare(strict_types=1);

namespace Controller\frontend;
use \Mlaphp\Response;
use \Domain\Pages\PageTransactions;

class AboutPage 
{
    protected PageTransactions $pageTransactions;
    protected Response $response;

    public function __construct(PageTransactions $pageTransactions, Response $response) {
        $this->pageTransactions = $pageTransactions;
        $this->response = $response;
    }

    public function run(): Response {
        // Daten holen
        $page = $this->pageTransactions->getData('aboutus');

        // Präsentation
        $this->response->setView('frontend/info.html.php');
        $this->response->setVars(array(
            'title' => 'About',
            'page' => $page
        ));
        return $this->response;
    }
}

?>
