<?php
declare(strict_types=1);

namespace Controller;
use \Mlaphp\Request;
use \Mlaphp\Response;

class NotFound 
{
    protected Request $request;
    protected Response $response;

    public function __construct(Request $request, Response $response) {
        $this->request = $request;
        $this->response = $response;
    }

    public function run(): Response {
        // URL parsen
        $urlPath = parse_url($this->request->server['REQUEST_URI'], PHP_URL_PATH);

        // Präsentation
        $this->response->setView('not-found.html.php');
        $this->response->setVars(array(
            'urlPath' => $urlPath
        ));  
        return $this->response;
    }
}

?>