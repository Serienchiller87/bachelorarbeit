<?php
require('../includes/config.php');
require('../includes/services.php');

// Session starten
session_start();

// Router erstellen & URL matchen
$path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$router = $di->get('router');
$route = $router->match($path);

// für Backend: Login Status prüfen
if((strpos($route, 'backend') !== false) && (strpos($route, 'LoginPage') === false) && (($_SESSION['login'] ?? '') == '')) {
    header('location: index.php');
    exit();
}

// Controller erstellen und ausführen
$controller = $di->newInstance($route);
$response = $controller->run();

// Bildschirm Ausgabe
$response->send();
?>
