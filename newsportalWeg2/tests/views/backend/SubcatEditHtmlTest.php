<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Mlaphp\Response;

class SubcatEditHtmlTest extends TestCase
{
    public function testView() {
        // Fake-Datensatz erstellen
        $subcat = array('id' => 3, 'CategoryId' => 9, 'CategoryName' => 'Sport', 'Subcategory' => 'Formula 1', 'SubCatDescription' => 'Lorem ipsum test.');
        $fakeCats = array(
            0 => array('id' => 8, 'CategoryName' => 'Lifestyle', 'Description' => 'Alles rund um Lifestyle'),
            1 => array('id' => 9, 'CategoryName' => 'Sport', 'Description' => 'Alles Sportliche gibt es hier')
        );

        // Response erstellen
        $response = new Response('../views');
        $response->setView('backend/subcatEdit.html.php');
        $response->setVars(array(
            'title' => 'Edit subcategory',
            'subcat' => $subcat,
            'cats' => $fakeCats,
            'msg' => $msg ?? ''
        ));

        // View in Buffer laden
        $result = $response->requireView();

        // Ergebnis prüfen
        $this->assertStringContainsString($subcat['SubCatDescription'], $result); // Daten in View gefunden?
    }
}

?>
