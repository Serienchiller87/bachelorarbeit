<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Mlaphp\Response;

class DashboardHtmlTest extends TestCase
{
    public function testView() {
        // Fake-Datensatz erstellen
        $fakeCats = array(
            0 => array('id' => 8, 'CategoryName' => 'Lifestyle', 'Description' => 'Alles rund um Lifestyle'),
            1 => array('id' => 9, 'CategoryName' => 'Sport', 'Description' => 'Alles Sportliche gibt es hier')
        );
        $fakeSubcats = array(
            0 => array('id' => 8, 'SubCategory' => 'Essen', 'SubCatDescription' => 'Lorem ipsum'),
            1 => array('id' => 9, 'SubCategory' => 'Trinken', 'SubCatDescription' => 'Lorem ipsum')
        );

        // Response erstellen
        $response = new Response('../views');
        $response->setView('backend/dashboard.html.php');
        $response->setVars(array(
            'title' => 'Dashboard',
            'cats' => $fakeCats,
            'subcats' => $fakeSubcats,
            'activeCount' => 483,
            'inactiveCount' => 2
        ));

        // View in Buffer laden
        $result = $response->requireView();

        // Ergebnis prüfen
        $this->assertStringContainsString('483', $result); // Daten in View gefunden?
    }
}

?>
