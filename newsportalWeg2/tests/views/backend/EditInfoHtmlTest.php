<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Mlaphp\Response;

class EditInfoHtmlTest extends TestCase
{
    public function testView() {
        // Fake-Datensatz erstellen
        $fakeRow = array('id' => 2, 'PageName' => 'contactus', 'PageTitle' => 'Contact Us Page', 'Description' => 'Some contact details.');

        // Response erstellen
        $response = new Response('../views');
        $response->setView('backend/info.html.php');
        $response->setVars(array(
            'title' => 'Contact',
            'page' => $fakeRow,
            'msg' => $msg ?? ''
        ));

        // View in Buffer laden
        $result = $response->requireView();

        // Ergebnis prüfen
        $this->assertStringContainsString($fakeRow['Description'], $result); // Daten in View gefunden?
    }
}

?>
