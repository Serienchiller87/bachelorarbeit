<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Mlaphp\Response;

class CommentsHtmlTest extends TestCase
{
    public function testView() {
        // Fake-Datensatz erstellen
        $fakeComments = array(
            0 => array('id' => 3, 'postId' => 3, 'PostTitle' => 'Börsennews', 'name' => 'Kilian', 'email' => 'randy@gmail.com', 'comment' => 'Seltsamer Beitrag.', 'postingDate' => '16.04.2020'),
            1 => array('id' => 5, 'postId' => 3, 'PostTitle' => 'Börsennews', 'name' => 'Lars', 'email' => 'lars@gmail.com', 'comment' => 'Dolor simet rasi', 'postingDate' => '18.04.2020')
        );

        // Response erstellen
        $response = new Response('../views');
        $response->setView('backend/comments.html.php');
        $response->setVars(array(
            'title' => 'Manage comments',
            'comments' => $fakeComments,
            'showActive' => true,
            'msg' => $msg ?? ''
        ));

        // View in Buffer laden
        $result = $response->requireView();

        // Ergebnis prüfen
        $this->assertStringContainsString($fakeComments[0]['comment'], $result); // Daten in View gefunden?
    }
}

?>
