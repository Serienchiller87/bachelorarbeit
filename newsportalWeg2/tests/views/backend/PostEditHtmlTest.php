<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Mlaphp\Response;

class PostEditHtmlTest extends TestCase
{
    public function testView() {
        // Fake-Datensatz erstellen
        $fakeRow = array('id' => 3, 'PostTitle' => 'Neues vom Mars', 'CategoryId' => 7, 'SubCategoryId' => 3, 'PostDetails' => 'Lorem ipsum test.', 'PostImage' => 'test1.jpg', 'CategoryName' => 'TestKategorie', 'SubCategory' => 'Formel1', 'PostingDate' => '15.03.2020');
        $fakeCats = array(
            0 => array('id' => 8, 'CategoryName' => 'Lifestyle', 'Description' => 'Alles rund um Lifestyle'),
            1 => array('id' => 9, 'CategoryName' => 'Sport', 'Description' => 'Alles Sportliche gibt es hier')
        );

        // Response erstellen
        $response = new Response('../views');
        $response->setView('backend/postEdit.html.php');
        $response->setVars(array(
            'title' => 'Edit post',
            'post' => $fakeRow,
            'cats' => $fakeCats,
            'msg' => $msg ?? ''
        ));

        // View in Buffer laden
        $result = $response->requireView();

        // Ergebnis prüfen
        $this->assertStringContainsString($fakeRow['PostDetails'], $result); // Daten in View gefunden?
    }
}

?>
