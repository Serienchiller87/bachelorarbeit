<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Mlaphp\Response;

class InfoHtmlTest extends TestCase
{
    public function testView() {
        // Fake-Datensatz erstellen
        $fakeRow = array('id' => 1, 'PageName' => 'aboutus', 'PageTitle' => 'About Us Page', 'Description' => 'This is a tiny description');

        // Response erstellen
        $response = new Response('../views');
        $response->setView('frontend/info.html.php');
        $response->setVars(array(
            'title' => 'About',
            'page' => $fakeRow
        ));

        // View in Buffer laden
        $result = $response->requireView();

        // Ergebnis prüfen
        $this->assertStringContainsString($fakeRow['Description'], $result); // Daten in View gefunden?
    }
}

?>
