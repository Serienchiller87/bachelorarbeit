<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Mlaphp\Response;

class DetailsHtmlTest extends TestCase
{
    public function testView() {
        // Fake-Datensatz erstellen
        $fakeRows = array(
            0 => array('id' => 3, 'PostTitle' => 'Neues vom Mars', 'CategoryId' => 7, 'SubCategoryId' => 3, 'PostDetails' => 'Lorem ipsum.', 'PostImage' => 'test1.jpg', 'CategoryName' => 'TestKategorie', 'SubCategory' => 'Formel1', 'PostingDate' => '15.03.2020'),
            1 => array('id' => 7, 'PostTitle' => 'Neues von Venus', 'CategoryId' => 6, 'SubCategoryId' => 9, 'PostDetails' => 'Lorem ipsum.', 'PostImage' => 'test2.jpg', 'CategoryName' => 'TestKategorie', 'SubCategory' => 'Formel3', 'PostingDate' => '16.03.2020')
        );
        $fakeCats = array(
            0 => array('id' => 8, 'CategoryName' => 'Lifestyle', 'Description' => 'Alles rund um Lifestyle'),
            1 => array('id' => 9, 'CategoryName' => 'Sport', 'Description' => 'Alles Sportliche gibt es hier')
        );
        $fakeComments = array(
            0 => array('id' => 3, 'postId' => 3, 'name' => 'Kilian', 'comment' => 'Seltsamer Beitrag.', 'postingDate' => '16.04.2020'),
            1 => array('id' => 5, 'postId' => 3, 'name' => 'Lars', 'comment' => 'Dolor simet rasi', 'postingDate' => '18.04.2020')
        );

        // Response erstellen
        $response = new Response('../views');
        $response->setView('frontend/details.html.php');
        $response->setVars(array(
            'title' => 'Details Page',
            'post' => $fakeRows[0],
            'comments' => $fakeComments,
            'catsSide' => $fakeCats,
            'postsSide' => $fakeRows
        ));

        // View in Buffer laden
        $result = $response->requireView();

        // Ergebnis prüfen
        $this->assertStringContainsString($fakeRows[0]['PostTitle'], $result); // Daten in View gefunden?
    }
}

?>
