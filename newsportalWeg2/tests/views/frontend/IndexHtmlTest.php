<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Mlaphp\Response;

class IndexHtmlTest extends TestCase
{
    public function testView() {
        // Fake-Datensatz erstellen
        $fakeRows = array(
            0 => array('id' => 3, 'PostTitle' => 'Neues vom Mars', 'CategoryId' => 7, 'SubCategoryId' => 3, 'PostDetails' => 'Lorem ipsum.', 'PostImage' => 'test1.jpg', 'CategoryName' => 'TestKategorie', 'PostingDate' => '15.03.2020'),
            1 => array('id' => 7, 'PostTitle' => 'Neues von Venus', 'CategoryId' => 6, 'SubCategoryId' => 9, 'PostDetails' => 'Lorem ipsum.', 'PostImage' => 'test2.jpg', 'CategoryName' => 'TestKategorie', 'PostingDate' => '16.03.2020')
        );
        $fakeCats = array(
            0 => array('id' => 8, 'CategoryName' => 'Lifestyle', 'Description' => 'Alles rund um Lifestyle'),
            1 => array('id' => 9, 'CategoryName' => 'Sport', 'Description' => 'Alles Sportliche gibt es hier')
        );

        // Response erstellen
        $response = new Response('../views');
        $response->setView('frontend/index.html.php');
        $response->setVars(array(
            'title' => 'Home Page',
            'pageno' => $_GET['pageno'] ?? 1,
            'totalPages' => 3,
            'posts' => $fakeRows,
            'catsSide' => $fakeCats,
            'postsSide' => $fakeRows
        ));

        // View in Buffer laden
        $result = $response->requireView();

        // Ergebnis prüfen
        $this->assertStringContainsString($fakeRows[0]['PostTitle'], $result); // Daten in View gefunden?
    }
}

?>
