<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Controller\backend\ContactPage;
use Domain\Pages\PageTransactions;
use Mlaphp\Response;

class ContactPageTest extends TestCase
{
    public function testRun() {
        // Fake-Datensatz erstellen
        $fakeRow = array('id' => 2, 'PageName' => 'contactus', 'PageTitle' => 'Contact Us Page');

        // Transactions-Stub erstellen
        $pageTransactions = $this->createMock(PageTransactions::class);
        $pageTransactions->method('getData')->willReturn($fakeRow);

        // Response erstellen
        $response = new Response('../views');

        // Objekt erstellen & Methode ausführen
        $page = new ContactPage($pageTransactions, $response);
        $result = $page->run();

        // Ergebnis prüfen
        $this->assertSame($result->getView(), 'backend/info.html.php'); // View File
        $this->assertSame($result->getVars()['page'], $fakeRow); // Daten
    }
}

?>
