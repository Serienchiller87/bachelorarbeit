<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Controller\backend\AddSubcatPage;
use Domain\Subcategories\SubcategoryTransactions;
use Domain\Categories\CategoryTransactions;
use Mlaphp\Response;

class AddSubcatPageTest extends TestCase
{
    public function testRun() {
        // Fake-Datensatz erstellen
        $fakeCats = array(
            0 => array('id' => 8, 'CategoryName' => 'Lifestyle', 'Description' => 'Alles rund um Lifestyle'),
            1 => array('id' => 9, 'CategoryName' => 'Sport', 'Description' => 'Alles Sportliche gibt es hier')
        );

        // CatTransactions-Stub erstellen
        $catTransactions = $this->createMock(CategoryTransactions::class);
        $catTransactions->method('showActiveCats')->willReturn($fakeCats);

        // SubcatTransactions-Stub erstellen
        $subcatTransactions = $this->createMock(SubcategoryTransactions::class);

        // Response erstellen
        $response = new Response('../views');

        // Objekt erstellen & Methode ausführen
        $page = new AddSubcatPage($subcatTransactions, $catTransactions, $response);
        $result = $page->run();

        // Ergebnis prüfen
        $this->assertSame($result->getView(), 'backend/subcatEdit.html.php'); // View File
        $this->assertSame($result->getVars()['cats'], $fakeCats); // Daten
    }
}

?>
