<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Controller\backend\EditPostPage;
use Domain\Posts\PostTransactions;
use Domain\Categories\CategoryTransactions;
use Mlaphp\Response;

class EditPostPageTest extends TestCase
{
    public function testRun() {
        // Fake-Datensatz erstellen
        $fakeRow = array('id' => 3, 'PostTitle' => 'Neues vom Mars', 'CategoryId' => 7, 'SubCategoryId' => 3, 'PostDetails' => 'Lorem ipsum.');
        $fakeCats = array(
            0 => array('id' => 8, 'CategoryName' => 'Lifestyle', 'Description' => 'Alles rund um Lifestyle'),
            1 => array('id' => 9, 'CategoryName' => 'Sport', 'Description' => 'Alles Sportliche gibt es hier')
        );

        // PostTransactions-Stub erstellen
        $postTransactions = $this->createMock(PostTransactions::class);
        $postTransactions->method('showPost')->willReturn($fakeRow);

        // CatTransactions-Stub erstellen
        $catTransactions = $this->createMock(CategoryTransactions::class);
        $catTransactions->method('showActiveCats')->willReturn($fakeCats);

        // Response erstellen
        $response = new Response('../views');

        // $_GET Variable zuweisen (postId)
        $_GET['nid'] = '3';

        // Objekt erstellen & Methode ausführen
        $page = new EditPostPage($postTransactions, $catTransactions, $response);
        $result = $page->run();

        // Ergebnis prüfen
        $this->assertSame($result->getView(), 'backend/postEdit.html.php'); // View File
        $this->assertSame($result->getVars()['post'], $fakeRow); // Daten
    }
}

?>
