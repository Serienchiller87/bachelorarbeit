<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Controller\backend\DashboardPage;
use Domain\Posts\PostTransactions;
use Domain\Categories\CategoryTransactions;
use Domain\Subcategories\SubcategoryTransactions;
use Mlaphp\Response;

class DashboardPageTest extends TestCase
{
    public function testRun() {
        // Fake-Datensatz erstellen
        $fakeCats = array(
            0 => array('id' => 8, 'CategoryName' => 'Lifestyle', 'Description' => 'Alles rund um Lifestyle'),
            1 => array('id' => 9, 'CategoryName' => 'Sport', 'Description' => 'Alles Sportliche gibt es hier')
        );
        $fakeSubcats = array(
            0 => array('id' => 8, 'SubCategory' => 'Essen', 'SubCatDescription' => 'Lorem ipsum'),
            1 => array('id' => 9, 'SubCategory' => 'Trinken', 'SubCatDescription' => 'Lorem ipsum')
        );
        $fakeRows = array(
            0 => array('id' => 3, 'PostTitle' => 'Neues vom Mars', 'CategoryId' => 7, 'SubCategoryId' => 3, 'PostDetails' => 'Lorem ipsum.'),
            1 => array('id' => 7, 'PostTitle' => 'Neues von Venus', 'CategoryId' => 6, 'SubCategoryId' => 9, 'PostDetails' => 'Lorem ipsum.')
        );

        // CatTransactions-Stub erstellen
        $catTransactions = $this->createMock(CategoryTransactions::class);
        $catTransactions->method('showActiveCats')->willReturn($fakeCats);

        // SubcatTransactions-Stub erstellen
        $subcatTransactions = $this->createMock(SubcategoryTransactions::class);
        $subcatTransactions->method('showAllActive')->willReturn($fakeSubcats);

        // PostTransactions-Stub erstellen
        $postTransactions = $this->createMock(PostTransactions::class);
        $postTransactions->method('showByActiveState')->willReturn($fakeRows);

        // Response erstellen
        $response = new Response('../views');

        // Objekt erstellen & Methode ausführen
        $page = new DashboardPage($postTransactions, $catTransactions, $subcatTransactions, $response);
        $result = $page->run();

        // Ergebnis prüfen
        $this->assertSame($result->getView(), 'backend/dashboard.html.php'); // View File
        $this->assertSame($result->getVars()['cats'], $fakeCats); // Daten
    }
}

?>
