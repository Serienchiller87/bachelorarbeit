<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Controller\backend\ManageCommentsPage;
use Domain\Comments\CommentTransactions;
use Mlaphp\Response;

class ManageCommentsPageTest extends TestCase
{
    public function testRun() {
        // Fake-Datensatz erstellen
        $fakeComments = array(
            0 => array('id' => 3, 'postId' => 3, 'name' => 'Kilian', 'comment' => 'Das ist ein seltsamer Beitrag.'),
            1 => array('id' => 5, 'postId' => 3, 'name' => 'Lars', 'comment' => 'Dolor simet rasi dolor simet rasi.')
        );

        // CommentTransactions-Stub erstellen
        $commentTransactions = $this->createMock(CommentTransactions::class);
        $commentTransactions->method('showAllActive')->willReturn($fakeComments);

        // Response erstellen
        $response = new Response('../views');

        // Objekt erstellen & Methode ausführen
        $page = new ManageCommentsPage($commentTransactions, $response);
        $result = $page->run();

        // Ergebnis prüfen
        $this->assertSame($result->getView(), 'backend/comments.html.php'); // View File
        $this->assertSame($result->getVars()['comments'], $fakeComments); // Daten
    }
}

?>
