<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Controller\frontend\AboutPage;
use Domain\Pages\PageTransactions;
use Mlaphp\Response;

class AboutPageTest extends TestCase
{
    public function testRun() {
        // Fake-Datensatz erstellen
        $fakeRow = array('id' => 1, 'PageName' => 'aboutus', 'PageTitle' => 'About Us Page');

        // Transactions-Stub erstellen
        $stub = $this->createMock(PageTransactions::class);
        $stub->method('getData')->willReturn($fakeRow);

        // Response erstellen
        $response = new Response('../views');

        // Objekt erstellen & Methode ausführen
        $page = new AboutPage($stub, $response);
        $result = $page->run();

        // Ergebnis prüfen
        $this->assertSame($result->getView(), 'frontend/info.html.php'); // View File
        $this->assertSame($result->getVars()['page']['PageTitle'], 'About Us Page'); // Daten
    }
}

?>
