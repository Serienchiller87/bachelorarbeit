<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Controller\frontend\DetailsPage;
use Domain\Posts\PostTransactions;
use Domain\Categories\CategoryTransactions;
use Domain\Comments\CommentTransactions;
use Mlaphp\Response;

class DetailsPageTest extends TestCase
{
    public function testRun() {
        // Fake-Datensätze erstellen
        $fakeRows = array(
            0 => array('id' => 3, 'PostTitle' => 'Neues vom Mars', 'CategoryId' => 7, 'SubCategoryId' => 3, 'PostDetails' => 'Lorem ipsum.'),
            1 => array('id' => 7, 'PostTitle' => 'Neues von Venus', 'CategoryId' => 6, 'SubCategoryId' => 9, 'PostDetails' => 'Lorem ipsum.')
        );
        $fakeCats = array(
            0 => array('id' => 8, 'CategoryName' => 'Lifestyle', 'Description' => 'Alles rund um Lifestyle'),
            1 => array('id' => 9, 'CategoryName' => 'Sport', 'Description' => 'Alles Sportliche gibt es hier')
        );
        $fakeComments = array(
            0 => array('id' => 3, 'postId' => 3, 'name' => 'Kilian', 'comment' => 'Das ist ein seltsamer Beitrag.'),
            1 => array('id' => 5, 'postId' => 3, 'name' => 'Lars', 'comment' => 'Dolor simet rasi dolor simet rasi.')
        );

        // PostTransactions-Stub erstellen
        $postTransactions = $this->createMock(PostTransactions::class);
        $postTransactions->method('showPost')->willReturn($fakeRows[0]);
        $postTransactions->method('indexPostsSidebar')->willReturn($fakeRows);

        // CatTransactions-Stub erstellen
        $catTransactions = $this->createMock(CategoryTransactions::class);
        $catTransactions->method('showActiveCats')->willReturn($fakeCats);

        // CommentTransactions-Stub erstellen
        $commentTransactions = $this->createMock(CommentTransactions::class);
        $commentTransactions->method('showActiveComments')->willReturn($fakeComments);

        // Response erstellen
        $response = new Response('../views');

        // $_GET Variable zuweisen (postId)
        $_GET['nid'] = '3';

        // Objekt erstellen & Methode ausführen
        $page = new DetailsPage($postTransactions, $catTransactions, $commentTransactions, $response);
        $result = $page->run();

        // Ergebnis prüfen
        $this->assertSame($result->getView(), 'frontend/details.html.php'); // View File
        $this->assertSame($result->getVars()['post'], $fakeRows[0]); // Daten
    }
}

?>
