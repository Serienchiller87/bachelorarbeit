<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Controller\frontend\IndexPage;
use Domain\Posts\PostTransactions;
use Domain\Categories\CategoryTransactions;
use Mlaphp\Response;

class IndexPageTest extends TestCase
{
    public function testRun() {
        // Fake-Datensätze erstellen
        $fakeRows = array(
            0 => array('id' => 3, 'PostTitle' => 'Neues vom Mars', 'CategoryId' => 7, 'SubCategoryId' => 3, 'PostDetails' => 'Lorem ipsum.'),
            1 => array('id' => 7, 'PostTitle' => 'Neues von Venus', 'CategoryId' => 6, 'SubCategoryId' => 9, 'PostDetails' => 'Lorem ipsum.')
        );
        $fakeCats = array(
            0 => array('id' => 8, 'CategoryName' => 'Lifestyle', 'Description' => 'Alles rund um Lifestyle'),
            1 => array('id' => 9, 'CategoryName' => 'Sport', 'Description' => 'Alles Sportliche gibt es hier')
        );

        // PostTransactions-Stub erstellen
        $postTransactions = $this->createMock(PostTransactions::class);
        $postTransactions->method('getTotalPages')->willReturn(3);
        $postTransactions->method('indexPosts')->willReturn($fakeRows);
        $postTransactions->method('indexPostsSidebar')->willReturn($fakeRows);

        // CatTransactions-Stub erstellen
        $catTransactions = $this->createMock(CategoryTransactions::class);
        $catTransactions->method('showActiveCats')->willReturn($fakeCats);

        // Response erstellen
        $response = new Response('../views');

        // Objekt erstellen & Methode ausführen
        $page = new IndexPage($postTransactions, $catTransactions, $response);
        $result = $page->run();

        // Ergebnis prüfen
        $this->assertSame($result->getView(), 'frontend/index.html.php'); // View File
        $this->assertSame($result->getVars()['totalPages'], 3); // Daten
    }
}

?>
