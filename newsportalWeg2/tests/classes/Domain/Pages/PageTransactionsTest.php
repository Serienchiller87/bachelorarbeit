<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Domain\Pages\PagesGateway;
use Domain\Pages\PageTransactions;

class PageTransactionsTest extends TestCase
{
    public function testGetData() {
        // Fake-Datensatz erstellen
        $fakeRow = array('id' => 1, 'PageName' => 'aboutus', 'PageTitle' => 'About Us Page');

        // Gateway-Stub erstellen
        $stub = $this->createMock(PagesGateway::class);
        $stub->method('selectByName')->willReturn($fakeRow);

        // Objekt erstellen & Methode ausführen
        $pageTransactions = new PageTransactions($stub);
        $result = $pageTransactions->getData('aboutus');

        // Ergebnis prüfen
        $this->assertSame($result['PageTitle'], 'About Us Page');
    }

    public function testUpdateData() {
        // Gateway-Stub erstellen
        $stub = $this->createMock(PagesGateway::class);
        $stub->method('update')->willReturn(true);

        // Objekt erstellen & Methode ausführen
        $pageTransactions = new PageTransactions($stub);
        $result = $pageTransactions->updateData('contactus', 'Contact Details', 'Here there are some details.');

        // Ergebnis prüfen
        $this->assertSame($result, true);
    }
}

?>
