<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Domain\Categories\CategoriesGateway;
use Domain\Categories\CategoryTransactions;

class CategoryTransactionsTest extends TestCase
{
    public function testShowCat() {
        // Fake-Datensatz erstellen
        $fakeRow = array('id' => 8, 'CategoryName' => 'Lifestyle', 'Description' => 'Alles rund um Lifestyle');

        // Gateway-Stub erstellen
        $stub = $this->createMock(CategoriesGateway::class);
        $stub->method('selectById')->willReturn($fakeRow);

        // Objekt erstellen & Methode ausführen
        $catTransactions = new CategoryTransactions($stub);
        $result = $catTransactions->showCat(8);

        // Ergebnis prüfen
        $this->assertSame($result['CategoryName'], 'Lifestyle');
    }

    public function testUpdateData() {
        // Gateway-Stub erstellen
        $stub = $this->createMock(CategoriesGateway::class);
        $stub->method('update')->willReturn(true);

        // Objekt erstellen & Methode ausführen
        $catTransactions = new CategoryTransactions($stub);
        $result = $catTransactions->updateData(8, 'Sport', 'Alles rund ums Thema Sport');

        // Ergebnis prüfen
        $this->assertSame($result, true);
    }

    public function testSetActive() {
        // Gateway-Stub erstellen
        $stub = $this->createMock(CategoriesGateway::class);
        $stub->method('setActiveState')->willReturn(true);

        // Objekt erstellen & Methode ausführen
        $catTransactions = new CategoryTransactions($stub);
        $result = $catTransactions->setActive(8, false);

        // Ergebnis prüfen
        $this->assertSame($result, true);
    }
}

?>
