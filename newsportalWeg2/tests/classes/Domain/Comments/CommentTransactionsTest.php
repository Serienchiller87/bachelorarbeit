<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Domain\Comments\CommentsGateway;
use Domain\Comments\CommentTransactions;

class CommentTransactionsTest extends TestCase
{
    public function testShowActiveComments() {
        // Fake-Datensätze erstellen
        $fakeRows = array(
            0 => array('id' => 2, 'postId' => 8, 'name' => 'Anuj', 'comment' => 'Finde ich gut!', 'status' => 1),
            1 => array('id' => 4, 'postId' => 8, 'name' => 'Kilian', 'comment' => 'Mag ich besonders.', 'status' => 1)
        );

        // Gateway-Stub erstellen
        $stub = $this->createMock(CommentsGateway::class);
        $stub->method('selectActiveByPostId')->willReturn($fakeRows);

        // Objekt erstellen & Methode ausführen
        $commentTransactions = new CommentTransactions($stub);
        $result = $commentTransactions->showActiveComments(8);

        // Ergebnis prüfen
        $this->assertCount(2, $result); // 2 FakeRows
    }

    public function testInsertData() {
        // Gateway-Stub erstellen
        $stub = $this->createMock(CommentsGateway::class);
        $stub->method('insert')->willReturn(true);

        // Objekt erstellen & Methode ausführen
        $commentTransactions = new CommentTransactions($stub);
        $_SESSION['token'] = bin2hex(random_bytes(32)); // CSRF Token zuweisen
        $result = $commentTransactions->insertData(8, 'Sebastian', 'sebi@gmail.com', 'Kann man so sehen...', $_SESSION['token']);

        // Ergebnis prüfen
        $this->assertSame($result, true);
    }

    public function testDeleteForever() {
        // Gateway-Stub erstellen
        $stub = $this->createMock(CommentsGateway::class);
        $stub->method('delete')->willReturn(true);

        // Objekt erstellen & Methode ausführen
        $commentTransactions = new CommentTransactions($stub);
        $result = $commentTransactions->deleteForever(2);

        // Ergebnis prüfen
        $this->assertSame($result, true);
    }
}

?>
