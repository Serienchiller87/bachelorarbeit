<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Domain\Posts\PostsGateway;
use Domain\Posts\PostTransactions;
define('RECORDSPERPAGE', 8);

class PostTransactionsTest extends TestCase
{
    public function testGetTotalPages() {
        // Gateway-Stub erstellen
        $stub = $this->createMock(PostsGateway::class);
        $stub->method('selectActivePostCount')->willReturn(25);

        // Objekt erstellen & Methode ausführen
        $postTransactions = new PostTransactions($stub);
        $result = $postTransactions->getTotalPages();

        // Ergebnis prüfen
        $this->assertSame($result, 4); // 4 Seiten für 25 Posts (8 pro Seite)
    }

    public function testIndexPosts() {
        // Fake-Datensätze erstellen
        $fakeRows = array(
            0 => array('id' => 3, 'PostTitle' => 'Neues vom Mars', 'CategoryId' => 7, 'SubCategoryId' => 3, 'PostDetails' => 'Lorem ipsum.'),
            1 => array('id' => 7, 'PostTitle' => 'Neues von Venus', 'CategoryId' => 6, 'SubCategoryId' => 9, 'PostDetails' => 'Lorem ipsum.'),
            2 => array('id' => 14, 'PostTitle' => 'Neues vom Mond', 'CategoryId' => 7, 'SubCategoryId' => 3, 'PostDetails' => 'Lorem ipsum.')
        );

        // Gateway-Stub erstellen
        $stub = $this->createMock(PostsGateway::class);
        $stub->method('selectActivePosts')->willReturn($fakeRows);

        // Objekt erstellen & Methode ausführen
        $postTransactions = new PostTransactions($stub);
        $result = $postTransactions->indexPosts();

        // Ergebnis prüfen
        $this->assertCount(3, $result); // 3 FakeRows
    }

    public function testShowPost() {
        // Fake-Datensatz erstellen
        $fakeRow = array('id' => 3, 'PostTitle' => 'Neues vom Mars', 'CategoryId' => 7, 'SubCategoryId' => 3, 'PostDetails' => 'Lorem ipsum.');

        // Gateway-Stub erstellen
        $stub = $this->createMock(PostsGateway::class);
        $stub->method('selectById')->willReturn($fakeRow);

        // Objekt erstellen & Methode ausführen
        $postTransactions = new PostTransactions($stub);
        $result = $postTransactions->showPost(3);

        // Ergebnis prüfen
        $this->assertSame($result['PostTitle'], 'Neues vom Mars');
    }

    public function testInsertPost() {
        // Gateway-Stub erstellen
        $stub = $this->createMock(PostsGateway::class);
        $stub->method('insert')->willReturn(true);

        // Objekt erstellen & Methode ausführen
        $postTransactions = new PostTransactions($stub);
        $result = $postTransactions->insertPost('Neue Infos Jupiter', 'halbmond.jpg', 'mock.jpg', 8, 12, 'Lorem ipsum.');

        // Ergebnis prüfen
        $this->assertSame($result, true);
    }

    public function testSetActive() {
        // Gateway-Stub erstellen
        $stub = $this->createMock(PostsGateway::class);
        $stub->method('setActiveState')->willReturn(true);

        // Objekt erstellen & Methode ausführen
        $postTransactions = new PostTransactions($stub);
        $result = $postTransactions->setActive(3, true);

        // Ergebnis prüfen
        $this->assertSame($result, true);
    }
}

?>
