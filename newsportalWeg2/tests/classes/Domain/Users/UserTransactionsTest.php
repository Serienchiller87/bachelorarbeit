<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Domain\Users\UsersGateway;
use Domain\Users\UserTransactions;

class UserTransactionsTest extends TestCase
{
    public function testLogin() {
        // Fake-Datensatz erstellen
        $fakeRow = array('id' => 1, 'AdminUserName' => 'admin', 'AdminPassword' => '$2y$12$KTAUryOMn/8x4UYbqHRwDecgYg4QIqA4jJOVhAPBTZBVVC8vrcaE2');

        // Gateway-Stub erstellen
        $stub = $this->createMock(UsersGateway::class);
        $stub->method('selectByName')->willReturn($fakeRow);

        // Objekt erstellen & Methode ausführen
        $userTransactions = new UserTransactions($stub);
        $result = $userTransactions->login('admin', 'Test@12345');

        // Ergebnis prüfen
        $this->assertSame($result, true);
    }

    public function testChangePassword() {
        // Fake-Datensatz erstellen
        $fakeRow = array('id' => 1, 'AdminUserName' => 'admin', 'AdminPassword' => '$2y$12$KTAUryOMn/8x4UYbqHRwDecgYg4QIqA4jJOVhAPBTZBVVC8vrcaE2');

        // Gateway-Stub erstellen
        $stub = $this->createMock(UsersGateway::class);
        $stub->method('selectByName')->willReturn($fakeRow);
        $stub->method('update')->willReturn(true);

        // Objekt erstellen & Methode ausführen
        $userTransactions = new UserTransactions($stub);
        $result = $userTransactions->changePassword('Test@12345', 'newPw');

        // Ergebnis prüfen
        $this->assertSame($result, true);
    }
}

?>
