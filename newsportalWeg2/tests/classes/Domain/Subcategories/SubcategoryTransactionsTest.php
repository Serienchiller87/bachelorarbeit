<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Domain\Subcategories\SubcategoriesGateway;
use Domain\Subcategories\SubcategoryTransactions;

class SubcategoryTransactionsTest extends TestCase
{
    public function testShowSubcat() {
        // Fake-Datensatz erstellen
        $fakeRow = array('id' => 10, 'CategoryId' => 8, 'Subcategory' => 'Welt', 'SubCatDescription' => 'Das aktuelle Weltgeschehen');

        // Gateway-Stub erstellen
        $stub = $this->createMock(SubcategoriesGateway::class);
        $stub->method('selectById')->willReturn($fakeRow);

        // Objekt erstellen & Methode ausführen
        $subcatTransactions = new SubcategoryTransactions($stub);
        $result = $subcatTransactions->showSubcat(10);

        // Ergebnis prüfen
        $this->assertSame($result['Subcategory'], 'Welt');
    }

    public function testUpdateData() {
        // Gateway-Stub erstellen
        $stub = $this->createMock(SubcategoriesGateway::class);
        $stub->method('update')->willReturn(true);

        // Objekt erstellen & Methode ausführen
        $subcatTransactions = new SubcategoryTransactions($stub);
        $result = $subcatTransactions->updateData(10, 8, 'Welt', 'Übersicht des Weltgeschehens');

        // Ergebnis prüfen
        $this->assertSame($result, true);
    }

    public function testSetActive() {
        // Gateway-Stub erstellen
        $stub = $this->createMock(SubcategoriesGateway::class);
        $stub->method('setActiveState')->willReturn(true);

        // Objekt erstellen & Methode ausführen
        $subcatTransactions = new SubcategoryTransactions($stub);
        $result = $subcatTransactions->setActive(10, false);

        // Ergebnis prüfen
        $this->assertSame($result, true);
    }
}

?>
