<?php
define('DB_SERVER','localhost');
define('DB_USER','root');
define('DB_PASS' ,'');
define('DB_NAME','newsportal75');
define('RECORDSPERPAGE', 8);
define('ERRORMESSAGE', 'Error: Something went wrong. Please try again.');

function getPDO(): PDO {
    // DB Verbindung herstellen
    try {
        $options = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        $result = new PDO('mysql:host=' . DB_SERVER . ';dbname=' . DB_NAME, DB_USER, DB_PASS, $options);
    } catch (PDOException $e) {
        throw new PDOException($e->getMessage(), (int)$e->getCode());
    }
    return $result;
}

// Autoloader
function autoloader($class) {
    // strip off any leading namespace separator from PHP 5.3
    $class = ltrim($class, '\\');

    // the eventual file path
    $subPath = '';

    // is there a PHP 5.3 namespace separator?
    $pos = strrpos($class, '\\');
    if($pos !== false) {
        // convert namespace separators to directory separators
        $ns = substr($class, 0, $pos);
        $subPath = str_replace('\\', DIRECTORY_SEPARATOR, $ns) . DIRECTORY_SEPARATOR;
        // remove the namespace portion from the final class name portion
        $class = substr($class, $pos + 1);
    }

    // convert underscores in the classname to directory separators
    $subPath .= str_replace('_', DIRECTORY_SEPARATOR, $class);

    // the path to our central class directory location 
    $dir = dirname(__DIR__) . '/classes';

    // prefix with the central directory location and suffix with .php, then require it
    $file = $dir . DIRECTORY_SEPARATOR . $subPath . '.php';
    require $file;
}

// register Autoloader
spl_autoload_register('autoloader');

// establish PDO Connection
$pdo = getPDO();
?>
