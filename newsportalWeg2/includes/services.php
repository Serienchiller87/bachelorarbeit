<?php
use Mlaphp\Di;
use Mlaphp\Router;
use Mlaphp\Response;
use Mlaphp\Request;

use Domain\Pages\PagesGateway;
use Domain\Pages\PageTransactions;
use Domain\Posts\PostsGateway;
use Domain\Posts\PostTransactions;
use Domain\Categories\CategoriesGateway;
use Domain\Categories\CategoryTransactions;
use Domain\Subcategories\SubcategoriesGateway;
use Domain\Subcategories\SubcategoryTransactions;
use Domain\Users\UsersGateway;
use Domain\Users\UserTransactions;
use Domain\Comments\CommentsGateway;
use Domain\Comments\CommentTransactions;

use Controller\NotFound;
use Controller\frontend\AboutPage;
use Controller\frontend\ContactPage;
use Controller\frontend\CategoryPage;
use Controller\frontend\IndexPage;
use Controller\frontend\DetailsPage;
use Controller\frontend\SearchPage;

use Controller\backend\AboutPage as EditAboutPage;
use Controller\backend\AddCatPage;
use Controller\backend\AddPostPage;
use Controller\backend\AddSubcatPage;
use Controller\backend\ChangeImgPage;
use Controller\backend\ChangePwdPage;
use Controller\backend\ContactPage as EditContactPage;
use Controller\backend\DashboardPage;
use Controller\backend\EditCatPage;
use Controller\backend\EditPostPage;
use Controller\backend\EditSubcatPage;
use Controller\backend\LoginPage;
use Controller\backend\ManageCatsPage;
use Controller\backend\ManageCommentsPage;
use Controller\backend\ManagePostsPage;
use Controller\backend\ManageSubcatsPage;
use Controller\backend\TrashPostsPage;
use Controller\backend\TrashCommentsPage;

// DI Container erstellen
$di = new Di($GLOBALS);

// Router erstellen
$di->set('router', function() use ($di) {
    $result = new Router(dirname(__DIR__) . '/pages');
    $result->setRoutes(array(
        '/not-found.php' => 'Controller\NotFound',
        '/about-us.php' => 'Controller\frontend\AboutPage',
        '/contact-us.php' => 'Controller\frontend\ContactPage',
        '/category.php' => 'Controller\frontend\CategoryPage',
        '/index.php' => 'Controller\frontend\IndexPage',
        '/' => 'Controller\frontend\IndexPage',
        '/news-details.php' => 'Controller\frontend\DetailsPage',
        '/search.php' => 'Controller\frontend\SearchPage',

        '/admin/aboutus.php' => 'Controller\backend\AboutPage',
        '/admin/add-category.php' => 'Controller\backend\AddCatPage',
        '/admin/add-post.php' => 'Controller\backend\AddPostPage',
        '/admin/add-subcategory.php' => 'Controller\backend\AddSubcatPage',
        '/admin/change-image.php' => 'Controller\backend\ChangeImgPage',
        '/admin/change-password.php' => 'Controller\backend\ChangePwdPage',
        '/admin/contactus.php' => 'Controller\backend\ContactPage',
        '/admin/dashboard.php' => 'Controller\backend\DashboardPage',
        '/admin/edit-category.php' => 'Controller\backend\EditCatPage',
        '/admin/edit-post.php' => 'Controller\backend\EditPostPage',
        '/admin/edit-subcategory.php' => 'Controller\backend\EditSubcatPage',
        '/admin/index.php' => 'Controller\backend\LoginPage',
        '/admin/logout.php' => 'Controller\backend\LogoutService',
        '/admin/manage-categories.php' => 'Controller\backend\ManageCatsPage',
        '/admin/manage-comments.php' => 'Controller\backend\ManageCommentsPage',
        '/admin/manage-posts.php' => 'Controller\backend\ManagePostsPage',
        '/admin/manage-subcategories.php' => 'Controller\backend\ManageSubcatsPage',
        '/admin/trash-posts.php' => 'Controller\backend\TrashPostsPage',
        '/admin/unapprove-comment.php' => 'Controller\backend\TrashCommentsPage'
    ));
    return $result;
});

// Response Objekt erstellen
$di->set('response', function() use ($di) {
    return new Response('../views');
});

// PageTransactions erstellen
$di->set('PageTransactions', function() use ($di) {
    $pagesGateway = new PagesGateway($di->pdo); 
    return new PageTransactions($pagesGateway);
});

// PostTransactions erstellen
$di->set('PostTransactions', function() use ($di) {
    $postsGateway = new PostsGateway($di->pdo);
    return new PostTransactions($postsGateway, $_GET['pageno'] ?? 1);
});

// CatTransactions erstellen
$di->set('CatTransactions', function() use ($di) {
    $catGateway = new CategoriesGateway($di->pdo);
    return new CategoryTransactions($catGateway);
});

// SubcatTransactions erstellen
$di->set('SubcatTransactions', function() use ($di) {
    $subcatGateway = new SubcategoriesGateway($di->pdo);
    return new SubcategoryTransactions($subcatGateway);
});

// UserTransactions erstellen
$di->set('UserTransactions', function() use ($di) {
    $usersGateway = new UsersGateway($di->pdo); 
    return new UserTransactions($usersGateway);
});

// CommentTransactions erstellen
$di->set('CommentTransactions', function() use ($di) {
    $commentsGateway = new CommentsGateway($di->pdo);
    return new CommentTransactions($commentsGateway);
});

// NotFound Seite erstellen
$di->set('Controller\NotFound', function() use ($di) {
    $request = new Request($GLOBALS);
    return new NotFound($request, $di->get('response'));
});

//////// Frontend ////////////////////////////////////////////////////

// AboutPage erstellen
$di->set('Controller\frontend\AboutPage', function() use ($di) {
    return new AboutPage($di->get('PageTransactions'), $di->get('response'));
});

// ContactPage erstellen
$di->set('Controller\frontend\ContactPage', function() use ($di) {
    return new ContactPage($di->get('PageTransactions'), $di->get('response'));
});

// CategoryPage erstellen
$di->set('Controller\frontend\CategoryPage', function() use ($di) {
    return new CategoryPage($di->get('PostTransactions'), $di->get('CatTransactions'), $di->get('response'));
});

// IndexPage erstellen
$di->set('Controller\frontend\IndexPage', function() use ($di) {
    return new IndexPage($di->get('PostTransactions'), $di->get('CatTransactions'), $di->get('response'));
});

// DetailsPage erstellen
$di->set('Controller\frontend\DetailsPage', function() use ($di) {
    return new DetailsPage($di->get('PostTransactions'), $di->get('CatTransactions'), $di->get('CommentTransactions'), $di->get('response'));
});

// SearchPage erstellen
$di->set('Controller\frontend\SearchPage', function() use ($di) {
    return new SearchPage($di->get('PostTransactions'), $di->get('CatTransactions'), $di->get('response'));
});

//////// Backend /////////////////////////////////////////////////////

// AboutPage erstellen
$di->set('Controller\backend\AboutPage', function() use ($di) {
    return new EditAboutPage($di->get('PageTransactions'), $di->get('response'));
});

// ContactPage erstellen
$di->set('Controller\backend\ContactPage', function() use ($di) {
    return new EditContactPage($di->get('PageTransactions'), $di->get('response'));
});

// AddCatPage erstellen
$di->set('Controller\backend\AddCatPage', function() use ($di) {
    return new AddCatPage($di->get('CatTransactions'), $di->get('response'));
});

// AddPostPage erstellen
$di->set('Controller\backend\AddPostPage', function() use ($di) {
    return new AddPostPage($di->get('PostTransactions'), $di->get('CatTransactions'), $di->get('response'));
});

// AddSubcatPage erstellen
$di->set('Controller\backend\AddSubcatPage', function() use ($di) {
    return new AddSubcatPage($di->get('SubcatTransactions'), $di->get('CatTransactions'), $di->get('response'));
});

// ChangeImgPage erstellen
$di->set('Controller\backend\ChangeImgPage', function() use ($di) {
    return new ChangeImgPage($di->get('PostTransactions'), $di->get('response'));
});

// ChangePwdPage erstellen
$di->set('Controller\backend\ChangePwdPage', function() use ($di) {
    return new ChangePwdPage($di->get('UserTransactions'), $di->get('response'));
});

// DashboardPage erstellen
$di->set('Controller\backend\DashboardPage', function() use ($di) {
    return new DashboardPage($di->get('PostTransactions'), $di->get('CatTransactions'), $di->get('SubcatTransactions'), $di->get('response'));
});

// EditCatPage erstellen
$di->set('Controller\backend\EditCatPage', function() use ($di) {
    return new EditCatPage($di->get('CatTransactions'), $di->get('response'));
});

// EditPostPage erstellen
$di->set('Controller\backend\EditPostPage', function() use ($di) {
    return new EditPostPage($di->get('PostTransactions'), $di->get('CatTransactions'), $di->get('response'));
});

// EditSubcatPage erstellen
$di->set('Controller\backend\EditSubcatPage', function() use ($di) {
    return new EditSubcatPage($di->get('SubcatTransactions'), $di->get('CatTransactions'), $di->get('response'));
});

// LoginPage erstellen
$di->set('Controller\backend\LoginPage', function() use ($di) {
    return new LoginPage($di->get('UserTransactions'), $di->get('response'));
});

// LogoutService erstellen
$di->set('Controller\backend\LogoutService', function() use ($di) {
    // Session leeren
    $_SESSION['login'] = '';
    session_unset();
    session_destroy();

    // Redirect auf Login-Seite
    header('location: index.php');
    exit();
});

// ManageCatsPage erstellen
$di->set('Controller\backend\ManageCatsPage', function() use ($di) {
    return new ManageCatsPage($di->get('CatTransactions'), $di->get('response'));
});

// ManageCommentsPage erstellen
$di->set('Controller\backend\ManageCommentsPage', function() use ($di) {
    return new ManageCommentsPage($di->get('CommentTransactions'), $di->get('response'));
});

// ManagePostsPage erstellen
$di->set('Controller\backend\ManagePostsPage', function() use ($di) {
    return new ManagePostsPage($di->get('PostTransactions'), $di->get('response'));
});

// ManageSubcatsPage erstellen
$di->set('Controller\backend\ManageSubcatsPage', function() use ($di) {
    return new ManageSubcatsPage($di->get('SubcatTransactions'), $di->get('response'));
});

// TrashPostsPage erstellen
$di->set('Controller\backend\TrashPostsPage', function() use ($di) {
    return new TrashPostsPage($di->get('PostTransactions'), $di->get('response'));
});

// TrashCommentsPage erstellen
$di->set('Controller\backend\TrashCommentsPage', function() use ($di) {
    return new TrashCommentsPage($di->get('CommentTransactions'), $di->get('response'));
});
?>
