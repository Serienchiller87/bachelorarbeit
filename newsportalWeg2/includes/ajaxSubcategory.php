<?php // Ajax Aufruf um dynamisch die Subkategorien nachzuladen (JSON aus Weg 1 übernommen)
use Domain\Subcategories\SubcategoriesGateway;
use Domain\Subcategories\SubcategoryTransactions;

require('config.php');

// Objekte erstellen
$subcatGateway = new SubcategoriesGateway($pdo);
$subcatTransactions = new SubcategoryTransactions($subcatGateway);

// Daten holen
if(!empty($_REQUEST["catid"])) {
    $result = array();
    $subcats = $subcatTransactions->showAllActiveByCat(intval($_REQUEST['catid']));
    foreach ($subcats as $value) { // JSON Objekte zusammenbauen
        $item = array();
        $item['key'] = $value['id'];
        $item['value'] = $value['Subcategory'];
        $result[] = $item;
    }
    echo json_encode($result, JSON_FORCE_OBJECT);
}
?>
