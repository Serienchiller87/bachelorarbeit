<?php require('header.html.php') ?>
<!-- Page Content -->
<div class="container" style="flex: 1;">
    <div class="row" style="margin-top: 4%">
        <!-- Blog Entries Column -->
        <div class="col-md-8">
            <!-- Blog Post -->
            <?php 
            if(($categoryName ?? '') != '') {
                echo '<h1>' . $this->esc($categoryName) .' News</h1><br>';
            }
            if(count($posts) < 1) {
                echo 'No record found...'; 
            } 
            foreach ($posts as $row) { 
            ?>
            <div class="card mb-4">
                <img class="card-img-top" src="admin/postimages/<?= $this->esc($row['PostImage']) ?>" alt="<?= $this->esc($row['PostTitle']) ?>">
                <div class="card-body">
                    <h2 class="card-title"><?= $this->esc($row['PostTitle']) ?></h2>
                    <p><b>Category : </b> <a href="category.php?catid=<?= $this->esc($row['CategoryId']) ?>"><?= $this->esc($row['CategoryName']) ?></a> </p>
                    <a href="news-details.php?nid=<?= $this->esc($row['id']) ?>" class="btn btn-primary">Read More &rarr;</a>
                </div>
                <div class="card-footer text-muted">
                    Posted on <?= $this->esc($row['PostingDate']) ?>
                </div>
            </div>
            <?php } ?>
            <!-- Pagination -->
            <ul class="pagination justify-content-center mb-4">
                <li class="page-item"><a href="?pageno=1"  class="page-link">First</a></li>
                <li class="<?php if($pageno <= 1){ echo 'disabled'; } ?> page-item">
                    <a href="<?php if($pageno <= 1){ echo '#'; } else { echo "?pageno=".($pageno - 1); } ?>" class="page-link">Prev</a>
                </li>
                <li class="<?php if($pageno >= $totalPages){ echo 'disabled'; } ?> page-item">
                    <a href="<?php if($pageno >= $totalPages){ echo '#'; } else { echo "?pageno=".($pageno + 1); } ?> " class="page-link">Next</a>
                </li>
                <li class="page-item"><a href="?pageno=<?= $totalPages ?>" class="page-link">Last</a></li>
            </ul>
        </div>
        <!-- Sidebar Widgets Column -->
        <?php require('sidebar.html.php') ?>
    </div>
    <!-- /.row -->
</div>
<!-- /.container -->
<?php require('footer.html.php') ?>
        