<?php require('header.html.php');?>
<!-- Page Content -->
<div class="container" style="flex: 1;">
    <h1 class="mt-4 mb-3"><?= $this->esc($page['PageTitle']) ?></h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.php">Home</a>
        </li>
        <li class="breadcrumb-item active"><?= $title ?></li>
    </ol>
    <!-- Intro Content -->
    <div class="row">
        <div class="col-lg-12">
            <p><?= $page['Description'] ?></p>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container -->
<?php require('footer.html.php') ?>
