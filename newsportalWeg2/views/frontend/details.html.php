<?php require('header.html.php') ?>
<!-- Page Content -->
<div class="container">
    <div class="row" style="margin-top: 4%">
        <!-- Blog Entries Column -->
        <div class="col-md-8">
            <!-- Blog Post -->
            <div class="card mb-4">     
                <div class="card-body">
                    <h2 class="card-title"><?= $this->esc($post['PostTitle']) ?></h2>
                    <p><b>Category : </b> <a href="category.php?catid=<?= $this->esc($post['CategoryId']) ?>"><?= $this->esc($post['CategoryName']) ?></a> |
                        <b>Sub Category : </b><?= $this->esc($post['SubCategory']) ?> <b> Posted on </b><?= $this->esc($post['PostingDate']) ?></p>
                    <hr />
                    <img class="img-fluid rounded" src="admin/postimages/<?= $this->esc($post['PostImage']) ?>" alt="<?= $this->esc($post['PostTitle']) ?>">
                    <p class="card-text">
                        <?= substr($post['PostDetails'], 0) ?>
                    </p>
                </div>
            </div>
        </div>
        <!-- Sidebar Widgets Column -->
        <?php require('sidebar.html.php') ?>
    </div>
    <!-- /.row -->
    <!---Comment Section --->
    <div class="row" style="margin-top: -8%">
        <div class="col-md-8">
            <div class="card my-4">
                <h5 class="card-header">Leave a Comment:</h5>
                <div class="card-body">
                    <form name="Comment" method="post">
                        <input type="hidden" name="csrftoken" value="<?= $this->esc($_SESSION['token']) ?>" />
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" placeholder="Enter your fullname" required>
                        </div>
                        <div class="form-group">
                            <input type="email" name="email" class="form-control" placeholder="Enter your Valid email" required>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="comment" rows="3" placeholder="Comment" required></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                    </form>
                </div>
            </div>
            <!---Comment Display Section --->
            <?php foreach ($comments as $value) { ?>
            <div class="media mb-4">
                <img class="d-flex mr-3 rounded-circle" src="images/usericon.png" alt="">
                <div class="media-body">
                    <h5 class="mt-0"><?= $this->esc($value['name']) ?> <br />
                        <span style="font-size:11px;"><b>at</b> <?= $this->esc($value['postingDate']) ?></span>
                    </h5>
                    <?= $this->esc($value['comment']) ?>            
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php require('footer.html.php') ?>
