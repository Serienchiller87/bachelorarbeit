<div class="row">
    <div class="col-sm-6">  
        <!---Success Message--->  
        <?php if($msg ?? '') { ?>
        <div class="alert alert-<?= strpos($msg, 'Error') !== false ? 'danger' : 'success' ?>" role="alert">
            <?= $this->esc($msg) ?>
        </div>
        <?php } ?>
    </div>
</div>