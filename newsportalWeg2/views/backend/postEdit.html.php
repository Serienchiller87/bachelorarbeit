<?php require('header.html.php') ?>
<!-- ========== Left Sidebar Start ========== -->
<?php require('sidebar.html.php') ?>
<!-- Left Sidebar End -->
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title"><?= $title ?></h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="#">Post</a>
                            </li>
                            <li>
                                <a href="#"><?= $title ?></a>
                            </li>
                            <li class="active">
                                <?= $title ?>
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <?php require('message.html.php') ?>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="p-6">
                        <div class="">
                            <form name="addpost" method="post" enctype="multipart/form-data">
                                <div class="form-group m-b-20">
                                    <label for="exampleInputEmail1">Post Title</label>
                                    <input type="text" class="form-control" id="posttitle" value="<?= $this->esc($post['PostTitle'] ?? '') ?>" name="posttitle" placeholder="Enter title" required>
                                </div>
                                <div class="form-group m-b-20">
                                    <label for="exampleInputEmail1">Category</label>
                                    <select class="form-control" name="category" id="category" onChange="getSubCat(this.value);" required>
                                        <option value="<?= $this->esc($post['CategoryId'] ?? '') ?>"><?= $this->esc($post['CategoryName'] ?? 'Select category') ?></option>
                                        <?php foreach ($cats as $value) { ?>
                                        <option value="<?= $this->esc($value['id']) ?>"><?= $this->esc($value['CategoryName']) ?></option>
                                        <?php } ?>
                                    </select> 
                                </div>
                                <div class="form-group m-b-20">
                                    <label for="exampleInputEmail1">Sub Category</label>
                                    <select class="form-control" name="subcategory" id="subcategory" required>
                                        <option value="<?= $this->esc($post['SubCategoryId'] ?? '') ?>"><?= $this->esc($post['SubCategory'] ?? 'Select subcategory') ?></option>
                                    </select> 
                                </div> 
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card-box">
                                            <h4 class="m-b-30 m-t-0 header-title"><b>Post Details</b></h4>
                                            <textarea class="summernote" name="postdescription" required><?= $this->esc($post['PostDetails'] ?? '') ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card-box">
                                            <h4 class="m-b-30 m-t-0 header-title"><b>Post image (*.jpg, *.jpeg, *.png, *.gif)</b></h4>
                                            <?php if(isset($post)) { ?>
                                            <img src="postimages/<?= $this->esc($post['PostImage']) ?>" width="300"/>
                                            <br />
                                            <a href="change-image.php?nid=<?= $this->esc($post['id']) ?>">Update Image</a>
                                            <?php } else { ?>
                                            <input type="file" class="form-control" id="postimage" name="postimage"  required>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" name="submit" class="btn btn-success waves-effect waves-light">Save</button>
                                <button type="button" class="btn btn-danger waves-effect waves-light">Discard</button>
                            </form>
                        </div>
                    </div> <!-- end p-20 -->
                </div> <!-- end col -->
            </div>
            <!-- end row -->
        </div> <!-- container -->
    </div> <!-- content -->
    <?php require('footer.html.php') ?>