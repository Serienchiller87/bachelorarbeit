<?php require('header.html.php') ?>
<!-- ========== Left Sidebar Start ========== -->
<?php require('sidebar.html.php') ?>
<!-- Left Sidebar End -->
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title"><?= $title ?></h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="#">Admin</a>
                            </li>
                            <li>
                                <a href="#">Category </a>
                            </li>
                            <li class="active">
                               <?= $title ?>
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <?php require('message.html.php') ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="demo-box m-t-20">
                        <div class="m-b-30">
                            <a href="add-category.php">
                                <button id="addToTable" class="btn btn-success waves-effect waves-light">Add <i class="mdi mdi-plus-circle-outline" ></i></button>
                            </a>
                        </div>
                        <div class="table-responsive">
                            <table class="table m-0 table-colored-bordered table-bordered-primary">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Category</th>
                                        <th>Description</th>
                                        <th>Posting Date</th>
                                        <th>Last updation Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $counter = 1;
                                    foreach ($activeCats as $value) { ?>
                                    <tr>
                                        <th scope="row"><?= $this->esc($counter) ?></th>
                                        <td><?= $this->esc($value['CategoryName']) ?></td>
                                        <td><?= $this->esc($value['Description']) ?></td>
                                        <td><?= $this->esc($value['PostingDate']) ?></td>
                                        <td><?= $this->esc($value['UpdationDate']) ?></td>
                                        <td><a href="edit-category.php?cid=<?= $this->esc($value['id']) ?>"><i class="fa fa-pencil" style="color: #29b6f6;"></i></a> 
                                            &nbsp;<a href="manage-categories.php?rid=<?= $this->esc($value['id']) ?>&&action=del"> <i class="fa fa-trash-o" style="color: #f05050"></i></a> </td>
                                    </tr>
                                    <?php $counter++; } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--- end row -->       
            <div class="row">
                <div class="col-md-12">
                    <div class="demo-box m-t-20">
                        <div class="m-b-30">
                            <h4><i class="fa fa-trash-o" ></i> Deleted Categories</h4>
                        </div>
                        <div class="table-responsive">
                            <table class="table m-0 table-colored-bordered table-bordered-danger">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th> Category</th>
                                        <th>Description</th>
                                        <th>Posting Date</th>
                                        <th>Last updation Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $counter = 1;
                                    foreach ($inactiveCats as $value) { ?>
                                    <tr>
                                        <th scope="row"><?= $this->esc($counter) ?></th>
                                        <td><?= $this->esc($value['CategoryName']) ?></td>
                                        <td><?= $this->esc($value['Description']) ?></td>
                                        <td><?= $this->esc($value['PostingDate']) ?></td>
                                        <td><?= $this->esc($value['UpdationDate']) ?></td>
                                        <td><a href="manage-categories.php?resid=<?= $this->esc($value['id']) ?>"><i class="ion-arrow-return-right" title="Restore this category"></i></a> 
                                            &nbsp;<a href="manage-categories.php?rid=<?= $this->esc($value['id']) ?>&&action=parmdel" title="Delete forever"> <i class="fa fa-trash-o" style="color: #f05050"></i> </td>
                                    </tr>
                                    <?php $counter++; } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>                  
        </div> <!-- container -->
    </div> <!-- content -->
    <?php require('footer.html.php') ?>