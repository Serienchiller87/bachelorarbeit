<?php require('header.html.php') ?>
<!-- Top Bar End -->
<!-- ========== Left Sidebar Start ========== -->
<?php require('sidebar.html.php') ?>
<!-- Left Sidebar End -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title"><?= $title ?></h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="#">Admin</a>
                            </li>
                            <li>
                                <a href="#">Category </a>
                            </li>
                            <li class="active">
                                <?= $title ?>
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title"><b><?= $title ?></b></h4>
                        <hr />
                        <?php require('message.html.php') ?>
                        <div class="row">
                            <div class="col-md-6">
                                <form class="form-horizontal" name="category" method="post">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Category</label>
                                        <div class="col-md-10">
                                            <select class="form-control" name="category" required>
                                                <option value="<?= $this->esc($subcat['CategoryId'] ?? '') ?>"><?= $this->esc($subcat['CategoryName'] ?? 'Select Category') ?></option>
                                                <?php foreach ($cats as $value) { ?>
                                                <option value="<?= $this->esc($value['id']) ?>"><?= $this->esc($value['CategoryName']) ?></option>
                                                <?php } ?>
                                            </select> 
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Sub-Category</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" value="<?= $this->esc($subcat['Subcategory'] ?? '') ?>" name="subcategory" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Sub-Category Description</label>
                                        <div class="col-md-10">
                                            <textarea class="form-control" rows="5" name="sucatdescription" required><?= $this->esc($subcat['SubCatDescription'] ?? '') ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">&nbsp;</label>
                                        <div class="col-md-10">
                                            <button type="submit" class="btn btn-custom waves-effect waves-light btn-md" name="submitsubcat">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->
        </div> <!-- container -->
    </div> <!-- content -->
    <?php require('footer.html.php') ?>