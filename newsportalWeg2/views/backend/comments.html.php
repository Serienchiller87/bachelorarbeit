<?php require('header.html.php') ?>
<!-- ========== Left Sidebar Start ========== -->
<?php require('sidebar.html.php') ?>
<!-- Left Sidebar End -->
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title"><?= $title ?></h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="#">Admin</a>
                            </li>
                            <li>
                                <a href="#">Comments </a>
                            </li>
                            <li class="active">
                                <?= $title ?>
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <?php require('message.html.php') ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="demo-box m-t-20">
                        <div class="table-responsive">
                            <table class="table m-0 table-colored-bordered table-bordered-primary">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th> Name</th>
                                        <th>Email Id</th>
                                        <th width="300">Comment</th>
                                        <th>Status</th>
                                        <th>Post / News</th>
                                        <th>Posting Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $counter = 1;
                                    foreach ($comments as $value) { ?>
                                    <tr>
                                        <th scope="row"><?= $this->esc($counter) ?></th>
                                        <td><?= $this->esc($value['name']) ?></td>
                                        <td><?= $this->esc($value['email']) ?></td>
                                        <td><?= $this->esc($value['comment']) ?></td>
                                        <td>Approved</td>
                                        <td><a href="edit-post.php?pid=<?= $this->esc($value['postId']) ?>"><?= $this->esc($value['PostTitle']) ?></a> </td>
                                        <td><?= $this->esc($value['postingDate']) ?></td>
                                        <td>
                                            <?php if($showActive) { ?>
                                            <a href="manage-comments.php?disid=<?= $this->esc($value['id']) ?>" title="Unapprove this comment"><i class="ion-arrow-return-right" style="color: #29b6f6;"></i></a> 
                                            <?php } else { ?>
                                            <a href="unapprove-comment.php?appid=<?= $this->esc($value['id']) ?>" title="Approve this comment"><i class="ion-arrow-return-right" style="color: #29b6f6;"></i></a>
                                            &nbsp;<a href="unapprove-comment.php?rid=<?= $this->esc($value['id']) ?>&&action=del" title="Delete this comment forever"> <i class="fa fa-trash-o" style="color: #f05050"></i></a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <?php $counter++; } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--- end row -->              
        </div> <!-- container -->
    </div> <!-- content -->
    <?php require('footer.html.php') ?>