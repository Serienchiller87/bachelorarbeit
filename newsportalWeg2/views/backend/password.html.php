<?php require('header.html.php') ?>
<!-- ========== Left Sidebar Start ========== -->
<?php require('sidebar.html.php') ?>
<!-- Left Sidebar End -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title"><?= $title ?></h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="#">Admin</a>
                            </li>
                            <li class="active">
                                <?= $title ?>
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title"><b><?= $title ?></b></h4>
                        <hr />
                        <?php require('message.html.php') ?>
                        <div class="row">
                            <div class="col-md-10">
                                <form class="form-horizontal" name="chngpwd" method="post" onSubmit="return valid();">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Current Password</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" value="" name="password" required>
                                        </div>
                                    </div>                                  
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">New Password</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" value="" name="newpassword" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Confirm Password</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" value="" name="confirmpassword" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">&nbsp;</label>
                                        <div class="col-md-8">
                                            <button type="submit" class="btn btn-custom waves-effect waves-light btn-md" name="submit">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->
        </div> <!-- container -->
    </div> <!-- content -->
    <?php require('footer.html.php') ?>