<?php require('header.html.php') ?>
<!-- ========== Left Sidebar Start ========== -->
<?php require('sidebar.html.php') ?>
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title"><?= $title ?></h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="#">Admin</a>
                            </li>
                            <li>
                                <a href="#">Posts</a>
                            </li>
                            <li class="active">
                                <?= $title ?> 
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <?php require('message.html.php') ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box">
                        <div class="table-responsive">
                            <table class="table table-colored table-centered table-inverse m-0">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Category</th>
                                        <th>Subcategory</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($posts as $value) { ?>
                                    <tr>
                                        <td><b><?= $this->esc($value['PostTitle']) ?></b></td>
                                        <td><?= $this->esc($value['CategoryName']) ?></td>
                                        <td><?= $this->esc($value['SubCategory']) ?></td>
                                        <td>
                                            <?php if($showActive) { ?>
                                            <a href="edit-post.php?nid=<?= $this->esc($value['id']) ?>" title="Edit this post"><i class="fa fa-pencil" style="color: #29b6f6;"></i></a>&nbsp;
                                            <a href="manage-posts.php?pid=<?= $this->esc($value['id']) ?>&&action=del" title="Delete this post"> <i class="fa fa-trash-o" style="color: #f05050"></i></a> 
                                            <?php } else { ?>
                                            <a href="trash-posts.php?pid=<?= $this->esc($value['id']) ?>&&action=restore"><i class="ion-arrow-return-right" title="Restore this Post"></i></a>&nbsp;
                                            <a href="trash-posts.php?presid=<?= $this->esc($value['id']) ?>&&action=perdel"><i class="fa fa-trash-o" style="color: #f05050" title="Permanently delete this post"></i></a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <?php } ?>                            
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- container -->
    </div> <!-- content -->
    <?php require('footer.html.php') ?>
