<?php require('header.html.php');?>
<!-- ========== Left Sidebar Start ========== -->
<?php require('sidebar.html.php');?>
<!-- Left Sidebar End -->
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title"><?= $title ?> Page</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="#">Pages</a>
                            </li>
                            <li class="active">
                                <?= $title ?> us
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <?php require('message.html.php') ?>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="p-6">
                        <div class="">
                            <form name="aboutus" method="post">
                                <div class="form-group m-b-20">
                                    <label for="exampleInputEmail1">Page Title</label>
                                    <input type="text" class="form-control" id="pagetitle" name="pagetitle" value="<?= $this->esc($page['PageTitle'])?>"  required>
                                </div>      
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card-box">
                                            <h4 class="m-b-30 m-t-0 header-title"><b>Page Details</b></h4>
                                            <textarea class="summernote" name="pagedescription"  required><?= $this->esc($page['Description'])?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" name="update" class="btn btn-success waves-effect waves-light">Update and Post</button>
                            </form>
                        </div>
                    </div> <!-- end p-20 -->
                </div> <!-- end col -->
            </div>
            <!-- end row -->
        </div> <!-- container -->
    </div> <!-- content -->
    <?php require('footer.html.php') ?>