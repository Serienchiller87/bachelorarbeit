<?php require('header.html.php') ?>
<!-- ========== Left Sidebar Start ========== -->
<?php require('sidebar.html.php') ?>
<!-- Left Sidebar End -->
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title"><?= $title ?></h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="#">Admin</a>
                            </li>
                            <li>
                                <a href="#"> Posts </a>
                            </li>
                               <li>
                                <a href="#"> Edit Posts </a>
                            </li>
                            <li class="active">
                               <?= $title ?>
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <?php require('message.html.php') ?>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="p-6">
                        <div class="">
                            <form name="addpost" method="post" enctype="multipart/form-data">
                                <div class="form-group m-b-20">
                                    <label for="exampleInputEmail1">Post Title</label>
                                    <input type="text" class="form-control" id="posttitle" value="<?= $this->esc($post['PostTitle']) ?>" name="posttitle" readonly>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card-box">
                                            <h4 class="m-b-30 m-t-0 header-title"><b>Current Post Image</b></h4>
                                            <img src="postimages/<?= $this->esc($post['PostImage']) ?>" width="300"/>
                                            <br />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card-box">
                                            <h4 class="m-b-30 m-t-0 header-title"><b>New post image (*.jpg, *.jpeg, *.png, *.gif)</b></h4>
                                            <input type="file" class="form-control" id="postimage" name="postimage" required>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" name="update" class="btn btn-success waves-effect waves-light">Update </button>
                            </form>
                        </div>
                    </div> <!-- end p-20 -->
                </div> <!-- end col -->
            </div>
            <!-- end row -->
        </div> <!-- container -->
    </div> <!-- content -->
    <?php require('footer.html.php') ?>