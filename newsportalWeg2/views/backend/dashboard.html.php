<?php require('header.html.php') ?>
<!-- ========== Left Sidebar Start ========== -->
<?php require('sidebar.html.php') ?>
<!-- Left Sidebar End -->
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title"><?= $title ?></h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="#">NewsPortal</a>
                            </li>
                            <li>
                                <a href="#">Admin</a>
                            </li>
                            <li class="active">
                                <?= $title ?>
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <a href="manage-categories.php">
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="card-box widget-box-one">
                            <i class="mdi mdi-chart-areaspline widget-one-icon"></i>
                            <div class="wigdet-one-content">
                                <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">Categories Listed</p>
                                <h2><?= $this->esc(count($cats)) ?> <small></small></h2>
                            </div>
                        </div>
                    </div>
                </a><!-- end col -->
                <a href="manage-subcategories.php">
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="card-box widget-box-one">
                            <i class="mdi mdi-layers widget-one-icon"></i>
                            <div class="wigdet-one-content">
                                <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="User This Month">Listed Subcategories</p>
                                <h2><?= $this->esc(count($subcats)) ?> <small></small></h2>
                            </div>
                        </div>
                    </div><!-- end col -->
                </a>
                <a href="manage-posts.php">                       
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="card-box widget-box-one">
                            <i class="mdi mdi-layers widget-one-icon"></i>
                            <div class="wigdet-one-content">
                                <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="User This Month">Live News</p>
                                <h2><?= $this->esc($activeCount) ?> <small></small></h2>
                            </div>
                        </div>
                    </div><!-- end col -->
                </a>
            </div>
            <!-- end row -->
            <div class="row">
                <a href="trash-posts.php"> 
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="card-box widget-box-one">
                            <i class="mdi mdi-layers widget-one-icon"></i>
                            <div class="wigdet-one-content">
                                <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="User This Month">Trash News</p>
                                <h2><?= $this->esc($inactiveCount) ?> <small></small></h2>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div> <!-- container -->
    </div> <!-- content -->
    <?php require('footer.html.php') ?>