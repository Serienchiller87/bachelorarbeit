<?php $this->header('HTTP/1.1 404 Not Found') ?>
<html>
    <head>
        <title>Not found</title>
    </head>
    <body>
        <h1>The requested page was not found on the server. Please try again.</h1>
        <p><?= $this->esc($urlPath) ?></p>
    </body>
</html>
