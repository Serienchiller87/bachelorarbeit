<?php
declare(strict_types=1);

class DetailsController extends AbstractController // Frontend
{
    protected $tpl = FRONTDIR . 'details.tpl.php';
    protected $dir404 = FRONTDIR . '404.tpl.php'; // @override wegen Ordnerstruktur

    public function __construct() {
        // CSRF Token erstellen
        if(empty($_SESSION['token'])) {
            $_SESSION['token'] = bin2hex(random_bytes(32));
        }
    }

    public function show() {
        // gewählten Post und Kommentare laden
        $post = Post::find(intval($_GET['nid']));
        if($post === false) $this->render404();
        $comments = Comment::findAllActiveByPost(intval($_GET['nid']));

        // Template laden
        $pageTitle = $post->getPostTitle();
        $url = 'index.php?controller=details&action=saveComment&nid=' . $_GET['nid']; // für Formular
        require_once($this->tpl);
    }

    public function saveComment() {
        // Kommentar speichern
        if(hash_equals($_SESSION['token'], $_POST['csrftoken'])) {
            $comment = new Comment();
            $comment->setPostId(intval($_GET['nid']));
            $comment->setName($_POST['name']);
            $comment->setEmail($_POST['email']);
            $comment->setComment($_POST['comment']);
            $comment->setIsActive(false);

            if($comment->save()) {
                $_SESSION['msg'] = 'Comment successfully submit. Your comment will be displayed after admin review.';
            } else {
                $_SESSION['msg'] = 'Error: Something went wrong. Please try again.';
            }
        }
        
        // erneut Template mit Erfolgsmeldung ausgeben
        header('Location: index.php?controller=details&action=show&nid=' . $_GET['nid']);
    }
}
?>
