<?php
declare(strict_types=1);

class CommentController extends AbstractController // Backend
{
    protected $tpl = BACKDIR . 'comments.tpl.php';
    protected $breadCrumb = 'Comments';

    public function indexActive() {
        // Werte zuweisen
        $comments = Comment::findAllActive();
        $showActiveComments = true;

        // Template laden
        $pageTitle = 'Approved Comments';
        require_once($this->tpl);
    }

    public function indexInactive() {
        // Werte zuweisen
        $comments = Comment::findAllInactive();
        $showActiveComments = false;

        // Template laden
        $pageTitle = 'Waiting for Approval';
        require_once($this->tpl);
    }

    public function setInactive() {
        // Comment inaktiv setzen
        $comment = Comment::find(intval($_GET['id']));
        if($comment === false) $this->render404();
        $comment->setIsActive(false);
        $comment->save();
        $_SESSION['msg'] = "Comment disapproved.";
        header('Location: index.php?controller=comment&action=indexActive');
    }

    public function restore() {
        // Restore Comment
        $comment = Comment::find(intval($_GET['id']));
        if($comment === false) $this->render404();
        $comment->setIsActive(true);
        $comment->save();
        $_SESSION['msg'] = "Comment approved.";
        header('Location: index.php?controller=comment&action=indexInactive');
    }

    public function delete() {
        // Comment delete forever
        $comment = Comment::find(intval($_GET['id']));
        if($comment === false) $this->render404();
        $comment->delete();
        $_SESSION['msg'] = "Comment deleted forever.";
        header('Location: index.php?controller=comment&action=indexInactive');
    }
}
?>
