<?php
declare(strict_types=1);

class IndexController extends AbstractController // Frontend
{
    protected $tpl = FRONTDIR . 'index.tpl.php';
    protected $pageNo = 1; // Pagination
    protected $offset = 0; 

    public function __construct() {
        // Offset aus PageNo berechnen
        $this->pageNo = intval($_GET['pageno'] ?? 1);
        $this->offset = ($this->pageNo - 1) * RECORDSPERPAGE;
    }

    public function index() {
        // alle aktiven Posts laden (desc)
        $posts = Post::findAllActiveOffset($this->offset);
        $totalPages = ceil(Post::countAllActive() / RECORDSPERPAGE);

        // Template laden
        $pageTitle = 'Home Page';
        $url = 'index.php?controller=index&action=index'; // für Pagination
        require_once($this->tpl);
    }

    public function category() {
        // alle aktiven Posts der Kategorie laden (desc)
        $posts = Post::findAllActiveByCatOffset(intval($_GET['catid']), $this->offset);
        $totalPages = ceil(Post::countAllActiveByCat(intval($_GET['catid'])) / RECORDSPERPAGE);
        $cat = Category::find(intval($_GET['catid']));
        if($cat === false) $this->render404();
        $categoryName = $cat->getCategoryName();

        // Template laden
        $pageTitle = 'Category Page';
        $url = 'index.php?controller=index&action=category&catid=' . $_GET['catid']; // für Pagination
        require_once($this->tpl);
    }

    public function search() {
        // alle aktiven Posts mit SuchString laden (desc)
        $posts = Post::findAllActiveByStrOffset($_REQUEST['searchtitle'], $this->offset);
        $totalPages = ceil(Post::countAllActiveByStr($_REQUEST['searchtitle']) / RECORDSPERPAGE);

        // Template laden
        $pageTitle = 'Search Page';
        $url = 'index.php?controller=index&action=search&searchtitle=' . $_REQUEST['searchtitle']; // für Pagination
        require_once($this->tpl);
    }
}
?>
