<?php
declare(strict_types=1);

class UserController extends AbstractController // Backend
{
    protected $indexTpl = BACKDIR . 'dashboard.tpl.php';
    protected $loginTpl = BACKDIR . 'login.tpl.php';
    protected $passwordTpl = BACKDIR . 'password.tpl.php';
    protected $breadCrumb = 'User';

    public function loginForm() {
        // Template laden
        $pageTitle = 'Admin login';
        require_once($this->loginTpl);
    }

    public function login() {
        // Login ausführen
        if(User::doLogin($_POST['username'], $_POST['password'])) {
            header('Location: index.php?controller=user&action=index');
        } else {
            $_SESSION['msg'] = "Error: Something went wrong with your login data.";
            header('Location: index.php?controller=user&action=loginForm');
            exit;
        }
    }

    public function logout() {
        // Session zerstören und zurück zur Startseite
        unset($_SESSION["login"]);
        header('Location: index.php?controller=user&action=loginForm');
    }

    public function index() {
        // Objekte zählen
        $countCat = count(Category::findAllActive());
        $countSubcat = count(Subcategory::findAllActive());
        $countPosts = Post::countAllActive(); 
        $countTrash = Post::countAllInactive();

        // Template laden
        $pageTitle = 'Dashboard';
        require_once($this->indexTpl);
    }

    public function passwordForm() {
        // Template laden
        $pageTitle = 'Change Password';
        require_once($this->passwordTpl);
    }

    public function changePassword() {
        // Passwort abspeichern (Validierung über JS im Template)
        $user = User::findByName($_SESSION['login']);
        if($user->changePassword($_POST['password'], $_POST['newpassword'])) {
            $_SESSION['msg'] = "Password changed successfully.";
        } else {
            $_SESSION['msg'] = "Error: Old password does not match.";
        }
        header('Location: index.php?controller=user&action=passwordForm');
    }
}
?>
