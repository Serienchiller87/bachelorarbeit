<?php
declare(strict_types=1);

class CategoryController extends AbstractController // Backend
{
    protected $indexTpl = BACKDIR . 'catIndex.tpl.php';
    protected $editTpl = BACKDIR . 'catEdit.tpl.php';
    protected $breadCrumb = 'Category';

    //////////////// Index Form ////////////////

    public function index() {
        // Category Objekte laden
        $activeCats = Category::findAllActive();
        $inactiveCats = Category::findAllInactive();

        // Template laden
        $pageTitle = 'Manage Categories';
        require_once($this->indexTpl);
    }

    public function setInactive() {
        // Category inaktiv setzen
        $cat = Category::find(intval($_GET['id']));
        if($cat === false) $this->render404();
        $cat->setIsActive(false);
        $cat->save();
        $_SESSION['msg'] = "Category deleted.";
        header('Location: index.php?controller=category&action=index');
    }

    public function restore() {
        // Category restore
        $cat = Category::find(intval($_GET['id']));
        if($cat === false) $this->render404();
        $cat->setIsActive(true);
        $cat->save();
        $_SESSION['msg'] = "Category restored successfully.";
        header('Location: index.php?controller=category&action=index');
    }

    public function delete() {
        // Category delete forever
        $cat = Category::find(intval($_GET['id']));
        if($cat === false) $this->render404();
        $cat->delete();
        $_SESSION['msg'] = "Category deleted forever.";
        header('Location: index.php?controller=category&action=index');
    }

    //////////////// Edit Form ////////////////

    public function add() {
        // Template laden
        $pageTitle = 'Add Category';
        require_once($this->editTpl);
    }

    public function edit(Category $cat = null) {
        // Category Objekt laden
        if(!isset($cat)) {
            $cat = Category::find(intval($_GET['id']));
            if($cat === false) $this->render404();
        }

        // Template laden
        $pageTitle = 'Edit Category';
        require_once($this->editTpl);
    }

    public function save() {
        // update oder insert?
        if($_POST['id'] !== '') {
            $cat = Category::find(intval($_POST['id']));
            if($cat === false) $this->render404();
            $action = 'updated';
        } else {
            $cat = new Category();
            $action = 'created';
        }

        // Werte zuweisen
        $cat->setCategoryName($_POST['category']);
        $cat->setDescription($_POST['description']);
        $cat->setIsActive(true);
        if($cat->save()) {
            $_SESSION['msg'] = "Category " . $action . " successfully.";
        } else {
            $_SESSION['msg'] = "Error: Something went wrong. Please try again.";
        }
        
        // Template mit Erfolgsmeldung aufrufen
        header('Location: index.php?controller=category&action=edit&id=' . $cat->getId());
    }
}
?>
