<?php
declare(strict_types=1);

class PostController extends AbstractController // Backend
{
    protected $indexTpl = BACKDIR . 'postIndex.tpl.php';
    protected $editTpl = BACKDIR . 'postEdit.tpl.php';
    protected $imageTpl = BACKDIR . 'image.tpl.php';
    protected $breadCrumb = 'Posts';

    //////////////// Index Form ////////////////

    public function indexActive() {
        // Post Objekte laden
        $posts = Post::findAllActive();
        $showActivePosts = true;

        // Template laden
        $pageTitle = 'Manage Posts';
        require_once($this->indexTpl);
    }

    public function indexInactive() {
        // Post Objekte laden
        $posts = Post::findAllInactive();
        $showActivePosts = false;

        // Template laden
        $pageTitle = 'Trash Posts';
        require_once($this->indexTpl);
    }

    public function setInactive() {
        // Post deaktivieren
        $post = Post::find(intval($_GET['id']));
        if($post === false) $this->render404();
        $post->setIsActive(false);
        $post->save();
        $_SESSION['msg'] = "Post was deleted successfully.";
        header('Location: index.php?controller=post&action=indexActive');
    }

    public function restore() {
        // restore Post
        $post = Post::find(intval($_GET['id']));
        if($post === false) $this->render404();
        $post->setIsActive(true);
        $post->save();
        $_SESSION['msg'] = "Post restored successfully.";
        header('Location: index.php?controller=post&action=indexInactive');
    }

    public function delete() {
        // delete Post forever
        $post = Post::find(intval($_GET['id']));
        if($post === false) $this->render404();
        $post->delete();
        $_SESSION['msg'] = "Post deleted forever.";
        header('Location: index.php?controller=post&action=indexInactive');
    }

    //////////////// Edit Form ////////////////

    public function add() {
        // Category Objekte laden
        $activeCats = Category::findAllActive();

        // Template laden
        $pageTitle = 'Add Post';
        require_once($this->editTpl);
    }

    public function edit(Post $post = null) {
        // aktuellen Post laden
        if(!isset($post)) {
            $post = Post::find(intval($_GET['id']));
            if($post === false) $this->render404();
        }
        // Category Objekte laden
        $activeCats = Category::findAllActive();
        
        // Template laden
        $pageTitle = 'Edit Post';
        require_once($this->editTpl);
    }

    public function save() {
        // update oder insert?
        if($_POST['id'] !== '') {
            $post = Post::find(intval($_POST['id']));
            if($post === false) $this->render404();
            // Bild Update in extra Funktion imageSave()
            $action = 'updated';
        } else {
            $post = new Post();
            $post->setPostImage($_FILES["postimage"]["name"]);
            $action = 'created';
        }

        // Werte zuweisen
        $post->setPostTitle($_POST['posttitle']);
        $post->setCategoryId(intval($_POST['category']));
        $post->setSubCategoryId(intval($_POST['subcategory']));
        $post->setPostDetails($_POST['postdescription']);
        $post->setIsActive(true);
        if($post->save()) {
            $_SESSION['msg'] = "Post successfully " . $action . ".";
        } else {
            $_SESSION['msg'] = "Error: Something went wrong. Please try again.";    
        } 

        // Template mit Erfolgsmeldung aufrufen
        header('Location: index.php?controller=post&action=edit&id=' . $post->getId());
    }

    public function image() {
        // Post Objekt laden
        $post = Post::find(intval($_GET['id']));
        if($post === false) $this->render404();

        // Template laden
        $pageTitle = 'Update Image';
        require_once($this->imageTpl);
    }

    public function saveImage() {
        // Image Update
        $post = Post::find(intval($_POST['id']));
        if($post === false) $this->render404();
        $post->setPostImage($_FILES["postimage"]["name"]);
        if($post->save()) {
            $_SESSION['msg'] = "Post image updated.";
        } else {
            $_SESSION['msg'] = "Error: Something went wrong. Please try again.";    
        } 

        // Template mit Erfolgsmeldung aufrufen
        header('Location: index.php?controller=post&action=edit&id=' . $post->getId());
    }
}
?>
