<?php
declare(strict_types=1);

abstract class AbstractController
{
    protected $dir404 = BACKDIR . '404.tpl.php';

    protected function render404() {
        // Fehlerseite bei nicht gefundenem Objekt
        header('HTTP/1.0 404 Not Found');
        $pageTitle = 'Error 404: Object not found!';
        require_once($this->dir404);
        die('Error 404');
    }
}
?>
