<?php
declare(strict_types=1);

class SubcategoryController extends AbstractController // Backend
{
    protected $indexTpl = BACKDIR . 'subcatIndex.tpl.php';
    protected $editTpl = BACKDIR . 'subcatEdit.tpl.php';
    protected $breadCrumb = 'Subcategory';

    //////////////// Index Form ////////////////

    public function index() {
        // Subcategory Objekte laden
        $activeSubcats = Subcategory::findAllActive();
        $inactiveSubcats = Subcategory::findAllInactive();

        // Template laden
        $pageTitle = 'Manage Subcategories';
        require_once($this->indexTpl);
    }

    public function setInactive() {
        // SubCategory inaktiv setzen
        $subcat = Subcategory::find(intval($_GET['id']));
        if($subcat === false) $this->render404();
        $subcat->setIsActive(false);
        $subcat->save();
        $_SESSION['msg'] = "Subcategory deleted.";
        header('Location: index.php?controller=subcategory&action=index');
    }

    public function restore() {
        // SubCategory restore
        $subcat = Subcategory::find(intval($_GET['id']));
        if($subcat === false) $this->render404();
        $subcat->setIsActive(true);
        $subcat->save();
        $_SESSION['msg'] = "Subcategory restored successfully.";
        header('Location: index.php?controller=subcategory&action=index');
    }

    public function delete() {
        // SubCategory delete forever
        $subcat = Subcategory::find(intval($_GET['id']));
        if($subcat === false) $this->render404();
        $subcat->delete();
        $_SESSION['msg'] = "Subcategory deleted forever.";
        header('Location: index.php?controller=subcategory&action=index');
    }

    //////////////// Edit Form ////////////////

    public function add() {
        // Category Objekte laden
        $activeCats = Category::findAllActive();
        $catId = -1;

        // Template laden
        $pageTitle = 'Add Subcategory';
        require_once($this->editTpl);
    }

    public function edit(Subcategory $subcat = null) {
        // Category Objekte laden
        $activeCats = Category::findAllActive();

        // Subcategory Objekt laden
        if(!isset($subcat)) {
            $subcat = Subcategory::find(intval($_GET['id']));
            if($subcat === false) $this->render404();
        }
        $catId = $subcat->getCategoryId();

        // Template laden
        $pageTitle = 'Edit Subcategory';
        require_once($this->editTpl);
    }

    public function save() {
        // update oder insert?
        if($_POST['id'] !== '') {
            $subcat = Subcategory::find(intval($_POST['id']));
            if($subcat === false) $this->render404();
            $action = 'updated';
        } else {
            $subcat = new Subcategory();
            $action = 'created';
        }

        // Werte zuweisen
        $subcat->setCategoryId(intval($_POST['category']));
        $subcat->setSubCatName($_POST['subcategory']);
        $subcat->setSubCatDescription($_POST['sucatdescription']);
        $subcat->setIsActive(true);
        if($subcat->save()) {
            $_SESSION['msg'] = "Subcategory " . $action . " successfully.";
        } else {
            $_SESSION['msg'] = "Error: Something went wrong. Please try again.";    
        } 

        // Template mit Erfolgsmeldung aufrufen
        header('Location: index.php?controller=subcategory&action=edit&id=' . $subcat->getId());
    }
}
?>
