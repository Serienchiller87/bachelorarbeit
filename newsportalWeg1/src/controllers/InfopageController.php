<?php
declare(strict_types=1);

class InfopageController extends AbstractController // Backend
{
    protected $tpl = BACKDIR . 'info.tpl.php';
    protected $breadCrumb = 'Info';

    public function aboutus() {
        // Page Objekt laden
        $page = Page::findByName('aboutus');

        // Template laden
        $pageTitle = 'Edit About Page';
        require_once($this->tpl);
    }

    public function contactus() {
        // Page Objekt laden
        $page = Page::findByName('contactus');

        // Template laden
        $pageTitle = 'Edit Contact Page';
        require_once($this->tpl);
    }

    public function save() {
        // Page abspeichern
        $pageType = $_POST['pagetype'];
        $page = Page::findByName($pageType);
        $page->setPageTitle($_POST['pagetitle']);
        $page->setDescription($_POST['pagedescription']);
        if($page->save()) {
            $_SESSION['msg'] = 'Page successfully updated.';
        } else {
            $_SESSION['msg'] = 'Error: Something went wrong. Please try again.'; 
        }
        
        // entsprechendes Template laden
        header('Location: index.php?controller=infopage&action=' . $pageType);
    }
}
?>
