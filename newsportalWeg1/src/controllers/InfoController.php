<?php
declare(strict_types=1);

class InfoController extends AbstractController // Frontend
{
    protected $tpl = FRONTDIR . 'info.tpl.php';

    public function about() {
        // Seite "aboutus" laden
        $page = Page::findByName('aboutus'); 

        // Template laden
        $pageTitle = 'About Page';
        require_once($this->tpl);
    }

    public function contact() {
        // Seite "contactus" laden
        $page = Page::findByName('contactus'); 

        // Template laden
        $pageTitle = 'Contact Page';
        require_once($this->tpl);
    }
}
?>
