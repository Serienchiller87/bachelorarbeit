<?php
declare(strict_types=1);

class Category extends AbstractModel
{
    ////////// Datenbankfelder //////////

    protected $CategoryName = '';
    protected $Description = '';
    protected $PostingDate = '0000-00-00 00:00:00'; // readOnly
    protected $UpdationDate = '0000-00-00 00:00:00'; // readOnly
    protected $Is_Active = 0;

    ////////// Getter und Setter //////////

    protected static function getTableName(): string { // Implementierung
        return 'tblcategory';
    }

    public function getCategoryName(): string {
        return htmlentities($this->CategoryName);
    }

    public function setCategoryName(string $aCategoryName) {
        $this->CategoryName = $aCategoryName;
    }

    public function getDescription(): string {
        return $this->Description;
    }

    public function setDescription(string $aDescription) {
        $this->Description = $aDescription;
    }

    public function getPostingDate(): string { // nur Getter - DB weist Wert zu 
        return $this->PostingDate;
    }

    public function getUpdationDate(): string { // nur Getter - DB weist Wert zu
        return $this->UpdationDate;
    }

    public function getIsActive(): bool {
        return boolval($this->Is_Active);
    }

    public function setIsActive(bool $aIsActive) {
        $this->Is_Active = intval($aIsActive);
    }

    ////////// Persistierung //////////

    public static function findAllActive(): array { 
        $stmt = self::$db->prepare('SELECT * FROM tblcategory WHERE Is_Active=1 ORDER BY CategoryName');
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Category');
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public static function findAllInactive(): array { 
        $stmt = self::$db->prepare('SELECT * FROM tblcategory WHERE Is_Active=0 ORDER BY CategoryName');
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Category');
        $stmt->execute();
        return $stmt->fetchAll();
    }

    protected function insert(): bool {
        $query = self::$db->prepare( 'INSERT INTO tblcategory (CategoryName, Description, Is_Active) 
                                      VALUES (:CategoryName, :Description, :Is_Active)');
        $data = get_object_vars($this);
        unset($data['id']); // generiert DB 
        unset($data['PostingDate']); // generiert DB 
        unset($data['UpdationDate']); // generiert DB 
        $result = $query->execute($data);
        $this->id = intval(self::$db->lastInsertId());
        return $result;
    }

    protected function update(): bool {
        $query = self::$db->prepare( 'UPDATE tblcategory SET 
                                      CategoryName=:CategoryName, Description=:Description, Is_Active=:Is_Active
                                      WHERE id=:id');
        $data = get_object_vars($this);
        unset($data['PostingDate']); // generiert DB 
        unset($data['UpdationDate']); // generiert DB 
        return $query->execute($data);
    }
}
