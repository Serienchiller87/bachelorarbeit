<?php
declare(strict_types=1);

class Page extends AbstractModel
{
    ////////// Datenbankfelder //////////

    protected $PageName = ''; 
    protected $PageTitle = '';
    protected $Description = '';
    protected $PostingDate = '0000-00-00 00:00:00'; // readOnly
    protected $UpdationDate = '0000-00-00 00:00:00'; // readOnly

    ////////// Getter und Setter //////////

    protected static function getTableName(): string { // Implementierung
        return 'tblpages';
    }

    public function getPageName(): string {
        return $this->PageName;
    }

    public function setPageName(string $aPageName) {
        $this->PageName = $aPageName;
    }

    public function getPageTitle(): string {
        return htmlentities($this->PageTitle);
    }

    public function setPageTitle(string $aPageTitle) {
        $this->PageTitle = $aPageTitle;
    }

    public function getDescription(): string {
        return $this->Description;
    }

    public function setDescription(string $aDescription) {
        $this->Description = $aDescription;
    }

    public function getPostingDate(): string { // nur Getter - DB weist Wert zu
        return $this->PostingDate;
    }

    public function getUpdationDate(): string { // nur Getter - DB weist Wert zu
        return $this->UpdationDate;
    }

    ////////// Persistierung //////////

    public static function findByName(string $aName): Page {
        $query = self::$db->prepare('SELECT * FROM tblpages WHERE PageName=:name');
        $query->execute(['name'=>$aName]);
        $query->setFetchMode(PDO::FETCH_CLASS, 'Page');
        return $query->fetch();
    }

    protected function insert(): bool {
        $query = self::$db->prepare( 'INSERT INTO tblpages (PageName, PageTitle, Description) 
                                      VALUES (:PageName, :PageTitle, :Description)');
        $data = get_object_vars($this);
        unset($data['id']); // generiert DB 
        unset($data['PostingDate']); // generiert DB 
        unset($data['UpdationDate']); // generiert DB
        $result = $query->execute($data);
        $this->id = intval(self::$db->lastInsertId());
        return $result;
    }

    protected function update(): bool {
        $query = self::$db->prepare( 'UPDATE tblpages SET 
                                      PageName=:PageName, PageTitle=:PageTitle, Description=:Description
                                      WHERE id=:id');
        $data = get_object_vars($this);
        unset($data['PostingDate']); // generiert DB 
        unset($data['UpdationDate']); // generiert DB
        return $query->execute($data);
    }
}
