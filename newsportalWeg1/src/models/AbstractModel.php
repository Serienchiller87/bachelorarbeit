<?php
declare(strict_types=1);

abstract class AbstractModel
{
    ////////// Datenbankfelder //////////

    protected $id = 0; // readOnly
    protected static $db;

    public function getId(): int { // nur Getter - DB weist Wert zu
        return $this->id;
    }

    ////////// Abstrakte Methoden //////////

    abstract protected static function getTableName();
    abstract protected function update();
    abstract protected function insert();

    ////////// Persistierung //////////

    public static function connect(PDO $db) {
        self::$db = $db;
    }

    public static function find(int $id) {
        $query = self::$db->prepare('SELECT * FROM ' . static::getTableName() . ' WHERE id=:id');
        $query->execute(['id'=>$id]);
        $query->setFetchMode(PDO::FETCH_CLASS, get_called_class());
        return $query->fetch();
    }

    public function save(): bool {
        if($this->getId() > 0) {
            return $this->update();
        } else {
            return $this->insert();
        }
    }

    public function delete() {
        $query = self::$db->prepare('DELETE FROM ' . static::getTableName() . ' WHERE id=:id');
        $query->execute(['id'=>$this->getId()]);
        $this->id = 0;
    }
}
