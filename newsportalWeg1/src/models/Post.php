<?php
declare(strict_types=1);

class Post extends AbstractModel
{
    ////////// Datenbankfelder //////////

    protected $PostTitle = '';
    protected $CategoryId = 0;
    protected $SubCategoryId = 0;
    protected $PostDetails = '';
    protected $PostingDate = '0000-00-00 00:00:00'; // readOnly
    protected $UpdationDate = '0000-00-00 00:00:00'; // readOnly
    protected $Is_Active = 0;
    protected $PostUrl = '';
    protected $PostImage = '';

    ////////// Getter und Setter ////////// 

    protected static function getTableName(): string { // Implementierung
        return 'tblposts';
    }

    public function getPostTitle(): string {
        return htmlentities($this->PostTitle);
    }

    public function setPostTitle(string $aPostTitle) {
        $this->PostTitle = $aPostTitle;
        // http://www.seanbehan.com/how-to-slugify-a-string-in-php/
        $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $aPostTitle), '-'));
        $this->PostUrl = $slug;
    }

    public function getCategoryId(): int {
        return $this->CategoryId;
    }

    public function setCategoryId(int $aCategoryId) {
        $this->CategoryId = $aCategoryId;
    }

    public function getSubCategoryId(): int {
        return $this->SubCategoryId;
    }

    public function setSubCategoryId(int $aSubCategoryId) {
        $this->SubCategoryId = $aSubCategoryId;
    }

    public function getPostDetails(): string {
        return $this->PostDetails;
    }

    public function setPostDetails(string $aPostDetails) {
        $this->PostDetails = $aPostDetails;
    }

    public function getPostingDate(): string { // nur Getter - DB weist Wert zu
        return htmlentities($this->PostingDate);
    }

    public function getUpdationDate(): string { // nur Getter - DB weist Wert zu
        return $this->UpdationDate;
    }

    public function getIsActive(): bool {
        return boolval($this->Is_Active);
    }

    public function setIsActive(bool $aIsActive) {
        $this->Is_Active = intval($aIsActive);
    }

    public function getPostUrl(): string { // nur Getter - Zuweisung erfolgt bei setPostTitle()
        return $this->PostUrl;
    }

    public function getPostImage(): string {
        return htmlentities($this->PostImage);
    }

    public function setPostImage(string $aPostImage) {
        // Bild in Ordner schieben und Verweis in DB speichern
        $extension = substr($aPostImage, strlen($aPostImage) - 4, strlen($aPostImage));
        $allowed_extensions = array(".jpg", "jpeg", ".png", ".gif");
        // Validation for allowed extensions 
        if(!in_array($extension, $allowed_extensions)) {
            echo "<script>alert('Invalid format. Only jpg / jpeg/ png /gif format allowed');</script>";
            $this->PostImage = '';
        } else {
            //rename the image file
            $newFileName = md5($aPostImage) . $extension;
            // Code for move image into directory
            move_uploaded_file($_FILES["postimage"]["tmp_name"], "postimages/" . $newFileName);
            $this->PostImage = $newFileName;
        }
    }

    ////////// Persistierung //////////

    public static function findAllActiveOffset(int $aOffset = 0): array { // Offset für Pagination
        $stmt = self::$db->prepare('SELECT * FROM tblposts WHERE Is_Active=1 ORDER BY id DESC LIMIT :offset, :recordsPerPage');
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Post');
        $stmt->execute(['offset' => $aOffset, 'recordsPerPage' => RECORDSPERPAGE]);
        return $stmt->fetchAll();
    }

    public static function findAllActive(): array { // alle anzeigen im Backend
        $stmt = self::$db->prepare('SELECT * FROM tblposts WHERE Is_Active=1');
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Post');
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public static function findAllInactive(): array { // alle anzeigen im Backend
        $stmt = self::$db->prepare('SELECT * FROM tblposts WHERE Is_Active=0');
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Post');
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public static function countAllActive(): int { // für Pagination Gesamtzahl erfassen
        return self::$db->query('SELECT COUNT(*) FROM tblposts WHERE Is_Active=1')->fetchColumn();
    }

    public static function countAllInactive(): int { // für Pagination Gesamtzahl erfassen
        return self::$db->query('SELECT COUNT(*) FROM tblposts WHERE Is_Active=0')->fetchColumn();
    }

    public static function findAllActiveByCatOffset(int $aCatId, int $aOffset = 0): array { // Offset für Pagination
        $stmt = self::$db->prepare('SELECT * FROM tblposts WHERE Is_Active=1 AND CategoryId=:catId ORDER BY id DESC LIMIT :offset, :recordsPerPage');
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Post');
        $stmt->execute(['catId' => $aCatId, 'offset' => $aOffset, 'recordsPerPage' => RECORDSPERPAGE]);
        return $stmt->fetchAll();
    }

    public static function countAllActiveByCat(int $aCatId): int { // für Pagination Gesamtzahl erfassen
        $stmt = self::$db->prepare('SELECT COUNT(*) FROM tblposts WHERE Is_Active=1 AND CategoryId=:catId');
        $stmt->execute(['catId' => $aCatId]);
        return $stmt->fetchColumn();
    }

    public static function findAllActiveByStrOffset(string $aStr, int $aOffset = 0): array { // Offset für Pagination
        $stmt = self::$db->prepare('SELECT * FROM tblposts WHERE Is_Active=1 AND PostTitle like :str ORDER BY id DESC LIMIT :offset, :recordsPerPage');
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Post');
        $stmt->execute(['str' => '%' . $aStr . '%', 'offset' => $aOffset, 'recordsPerPage' => RECORDSPERPAGE]);
        return $stmt->fetchAll();
    }

    public static function countAllActiveByStr(string $aStr): int { // für Pagination Gesamtzahl erfassen
        $stmt = self::$db->prepare('SELECT COUNT(*) FROM tblposts WHERE Is_Active=1 AND PostTitle like :str');
        $stmt->execute(['str' => '%' . $aStr . '%']);
        return $stmt->fetchColumn();
    }

    protected function insert(): bool {
        $query = self::$db->prepare( 'INSERT INTO tblposts
                                      (PostTitle, CategoryId, SubCategoryId, PostDetails, Is_Active, PostUrl, PostImage) 
                                      VALUES 
                                      (:PostTitle, :CategoryId, :SubCategoryId, :PostDetails, :Is_Active, :PostUrl, :PostImage)');
        $data = get_object_vars($this);
        unset($data['id']); // generiert DB 
        unset($data['PostingDate']); // generiert DB 
        unset($data['UpdationDate']); // generiert DB 
        $result = $query->execute($data);
        $this->id = intval(self::$db->lastInsertId());
        return $result;
    }

    protected function update(): bool {
        $query = self::$db->prepare( 'UPDATE tblposts SET 
                                      PostTitle=:PostTitle, CategoryId=:CategoryId, SubCategoryId=:SubCategoryId, 
                                      PostDetails=:PostDetails, Is_Active=:Is_Active, PostUrl=:PostUrl, PostImage=:PostImage
                                      WHERE id=:id');
        $data = get_object_vars($this);
        unset($data['PostingDate']); // generiert DB 
        unset($data['UpdationDate']); // generiert DB 
        return $query->execute($data);
    }
}
