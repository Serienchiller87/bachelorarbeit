<?php
declare(strict_types=1);

class Comment extends AbstractModel
{
    ////////// Datenbankfelder //////////

    protected $postId = 0; 
    protected $name = '';
    protected $email = '';
    protected $comment = '';
    protected $postingDate = '0000-00-00 00:00:00'; // readOnly
    protected $status = 0;

    ////////// Getter und Setter //////////

    protected static function getTableName(): string { // Implementierung
        return 'tblcomments';
    }

    public function getPostId(): int {
        return intval($this->postId);
    }

    public function setPostId(int $aPostId) {
        $this->postId = $aPostId;
    }

    public function getName(): string {
        return htmlentities($this->name);
    }

    public function setName(string $aName) {
        $this->name = $aName;
    }

    public function getEmail(): string {
        return $this->email;
    }

    public function setEmail(string $aEmail) {
        $this->email = $aEmail;
    }

    public function getComment(): string {
        return htmlentities($this->comment);
    }

    public function setComment(string $aComment) {
        $this->comment = $aComment;
    }

    public function getPostingDate(): string { // nur Getter - DB weist Wert zu 
        return htmlentities($this->postingDate);
    }

    public function getIsActive(): bool {
        return boolval($this->status);
    }

    public function setIsActive(bool $aIsActive) {
        $this->status = intval($aIsActive);
    }

    ////////// Persistierung //////////

    public static function findAllActiveByPost(int $aPostId): array { 
        $stmt = self::$db->prepare('SELECT * FROM tblcomments WHERE status=1 AND postId=:postId ORDER BY id DESC');
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Comment');
        $stmt->execute(['postId' => $aPostId]);
        return $stmt->fetchAll();
    }

    public static function findAllActive(): array { 
        $stmt = self::$db->prepare('SELECT tblcomments.* FROM tblcomments 
                                    JOIN tblposts ON tblcomments.postId=tblposts.id 
                                    WHERE tblcomments.status=1 ORDER BY id DESC');
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Comment');
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public static function findAllInactive(): array { 
        $stmt = self::$db->prepare('SELECT tblcomments.* FROM tblcomments 
                                    JOIN tblposts ON tblcomments.postId=tblposts.id 
                                    WHERE tblcomments.status=0 ORDER BY id DESC');
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Comment');
        $stmt->execute();
        return $stmt->fetchAll();
    }

    protected function insert(): bool {
        $query = self::$db->prepare( 'INSERT INTO tblcomments (postId, name, email, comment, status) 
                                      VALUES (:postId, :name, :email, :comment, :status)');
        $data = get_object_vars($this);
        unset($data['id']); // generiert DB 
        unset($data['postingDate']); // generiert DB 
        $result = $query->execute($data);
        $this->id = intval(self::$db->lastInsertId());
        return $result;
    }

    protected function update(): bool {
        $query = self::$db->prepare( 'UPDATE tblcomments SET 
                                      postId=:postId, name=:name, email=:email, comment=:comment, status=:status 
                                      WHERE id=:id');
        $data = get_object_vars($this);
        unset($data['postingDate']); // generiert DB
        return $query->execute($data);
    }
}
