<?php
declare(strict_types=1);

class User extends AbstractModel
{
    ////////// Datenbankfelder //////////

    protected $AdminUserName = '';
    protected $AdminPassword = '';
    protected $AdminEmailId = '';
    protected $Is_Active = 0;

    ////////// Getter und Setter //////////

    protected static function getTableName(): string { // Implementierung
        return 'tbladmin';
    }

    public function getAdminUserName(): string {
        return $this->AdminUserName;
    }

    public function setAdminUserName(string $aAdminUserName) {
        $this->AdminUserName = $aAdminUserName;
    }

    public function getAdminPassword(): string {
        return htmlentities($this->AdminPassword);
    }

    public function setAdminPassword(string $aAdminPassword) {
        $hashedPassword = password_hash($aAdminPassword, PASSWORD_BCRYPT, ['cost' => 12]);
        $this->AdminPassword = $hashedPassword;
    }

    public function getAdminEmailId(): string {
        return $this->AdminEmailId;
    }

    public function setAdminEmailId(string $aAdminEmailId) {
        $this->AdminEmailId = $aAdminEmailId;
    }

    public function getIsActive(): bool {
        return boolval($this->Is_Active);
    }

    public function setIsActive(bool $aIsActive) {
        $this->Is_Active = intval($aIsActive);
    }

    ////////// Persistierung //////////

    public static function findByName(string $aName): User {
        $query = self::$db->prepare('SELECT * FROM tbladmin WHERE AdminUserName=:name');
        $query->execute(['name'=>$aName]);
        $query->setFetchMode(PDO::FETCH_CLASS, 'User');
        return $query->fetch();
    }

    public function changePassword(string $aOld, string $aNew): bool {
        $result = false;
        if(password_verify($aOld, $this->getAdminPassword())) {
            $this->setAdminPassword($aNew);
            $result = $this->save();
        }
        return $result;
    }

    public static function doLogin(string $aName, string $aPassword): bool {
        $result = false;
        $user = self::findByName($aName);
        if ($user instanceof User) {
            $result = password_verify($aPassword, $user->getAdminPassword());
        }
        if($result) {
            $_SESSION['login'] = $aName;
        }
        return $result;
    }

    protected function insert(): bool { // Implementierung
        $query = self::$db->prepare( 'INSERT INTO tbladmin (AdminUserName, AdminPassword, AdminEmailId, Is_Active) 
                                      VALUES (:AdminUserName, :AdminPassword, :AdminEmailId, :Is_Active)');
        $data = get_object_vars($this);
        unset($data['id']); // generiert DB 
        unset($data['CreationDate']); // generiert DB 
        unset($data['UpdationDate']); // generiert DB 
        $result = $query->execute($data);
        $this->id = intval(self::$db->lastInsertId());
        return $result;
    }

    protected function update(): bool { // Implementierung
        $query = self::$db->prepare( 'UPDATE tbladmin SET 
                                      AdminUserName=:AdminUserName, AdminPassword=:AdminPassword, 
                                      AdminEmailId=:AdminEmailId, Is_Active=:Is_Active
                                      WHERE id=:id');
        $data = get_object_vars($this);
        unset($data['CreationDate']); // generiert DB 
        unset($data['UpdationDate']); // generiert DB 
        return $query->execute($data);
    }
}
