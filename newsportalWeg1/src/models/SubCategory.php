<?php
declare(strict_types=1);

class Subcategory extends AbstractModel
{
    ////////// Datenbankfelder //////////

    protected $CategoryId = 0;
    protected $Subcategory = '';
    protected $SubCatDescription = '';
    protected $PostingDate = '0000-00-00 00:00:00'; // readOnly
    protected $UpdationDate = '0000-00-00 00:00:00'; // readOnly
    protected $Is_Active = 0;

    ////////// Getter und Setter //////////

    protected static function getTableName(): string { // Implementierung
        return 'tblsubcategory';
    }

    public function getCategoryId(): int {
        return $this->CategoryId;
    }

    public function setCategoryId(int $aCategoryId) {
        $this->CategoryId = $aCategoryId;
    }

    public function getSubCatName(): string {
        return htmlentities($this->Subcategory);
    }

    public function setSubCatName(string $aSubcatName) {
        $this->Subcategory = $aSubcatName;
    }

    public function getSubCatDescription(): string {
        return $this->SubCatDescription;
    }

    public function setSubCatDescription(string $aSubCatDescription) {
        $this->SubCatDescription = $aSubCatDescription;
    }

    public function getPostingDate(): string { // nur Getter - DB weist Wert zu
        return $this->PostingDate;
    }

    public function getUpdationDate(): string { // nur Getter - DB weist Wert zu
        return $this->UpdationDate;
    }

    public function getIsActive(): bool {
        return boolval($this->Is_Active);
    }

    public function setIsActive(bool $aIsActive) {
        $this->Is_Active = intval($aIsActive);
    }

    ////////// Persistierung //////////

    public static function findAllActive(): array { 
        $stmt = self::$db->prepare('SELECT tblsubcategory.* FROM tblsubcategory 
                                    JOIN tblcategory ON tblsubcategory.CategoryId=tblcategory.id 
                                    WHERE tblsubcategory.Is_Active=1 
                                    ORDER BY Subcategory');
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Subcategory');
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public static function findAllActiveByCat(int $aCatId): array { 
        $stmt = self::$db->prepare('SELECT * FROM tblsubcategory 
                                    WHERE Is_Active=1 AND CategoryId=:catId
                                    ORDER BY Subcategory');
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Subcategory');
        $stmt->execute(['catId' => $aCatId]);
        return $stmt->fetchAll();
    }

    public static function findAllInactive(): array { 
        $stmt = self::$db->prepare('SELECT tblsubcategory.* FROM tblsubcategory 
                                    JOIN tblcategory ON tblsubcategory.CategoryId=tblcategory.id 
                                    WHERE tblsubcategory.Is_Active=0 
                                    ORDER BY Subcategory');
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Subcategory');
        $stmt->execute();
        return $stmt->fetchAll();
    }

    protected function insert(): bool {
        $query = self::$db->prepare( 'INSERT INTO tblsubcategory (CategoryId, Subcategory, SubCatDescription, Is_Active) 
                                      VALUES (:CategoryId, :Subcategory, :SubCatDescription, :Is_Active)');
        $data = get_object_vars($this);
        unset($data['id']); // generiert DB 
        unset($data['PostingDate']); // generiert DB 
        unset($data['UpdationDate']); // generiert DB 
        $result = $query->execute($data);
        $this->id = intval(self::$db->lastInsertId());
        return $result;
    }

    protected function update(): bool {
        $query = self::$db->prepare( 'UPDATE tblsubcategory SET 
                                      CategoryId=:CategoryId, Subcategory=:Subcategory, 
                                      SubCatDescription=:SubCatDescription, Is_Active=:Is_Active
                                      WHERE id=:id');
        $data = get_object_vars($this);
        unset($data['PostingDate']); // generiert DB 
        unset($data['UpdationDate']); // generiert DB 
        return $query->execute($data);
    }
}
