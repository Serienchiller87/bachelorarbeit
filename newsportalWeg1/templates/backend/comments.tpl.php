<?php require_once(__DIR__ . '/inc/header.tpl.php');?>
<?php require_once(__DIR__ . '/inc/sidebar.tpl.php');?>
<?php require_once(__DIR__ . '/inc/top.tpl.php');?>
<?php require_once(__DIR__ . '/inc/message.tpl.php');?>
                                
                        <div class="row">
                            <div class="col-md-12">
                                <div class="demo-box m-t-20">
                                    <div class="table-responsive">
                                        <table class="table m-0 table-colored-bordered table-bordered-primary">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th> Name</th>
                                                    <th>Email Id</th>
                                                    <th width="300">Comment</th>
                                                    <th>Status</th>
                                                    <th>Post / News</th>
                                                    <th>Posting Date</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
<?php 
$cnt=1;
foreach ($comments as $value) {
?>
    <tr>
        <th scope="row"><?php echo $cnt;?></th>
        <td><?= $value->getName() ?></td>
        <td><?= $value->getEmail() ?></td>
        <td><?= $value->getComment() ?></td>
        <td><?= $showActiveComments ? 'Approved' : 'Unapproved' ?></td>

        <td><a href="index.php?controller=post&action=edit&id=<?= $value->getId() ?>"><?= Post::find($value->getPostId())->getPostTitle() ?></a> </td>
        <td><?= $value->getPostingDate() ?></td>
        <td class="text-center">
<?php if($showActiveComments) { ?>
            <a href="index.php?controller=comment&action=setInactive&id=<?= $value->getId() ?>" title="Disapprove this comment"><i class="ion-arrow-return-right" style="color: #29b6f6;"></i></a>
<?php } else { ?>
            <a href="index.php?controller=comment&action=restore&id=<?= $value->getId() ?>" title="Approve this comment"><i class="ion-arrow-return-right" style="color: #29b6f6;"></i></a> 
            &nbsp;<a href="index.php?controller=comment&action=delete&id=<?= $value->getId() ?>" title="Delete comment forever"> <i class="fa fa-trash-o" style="color: #f05050"></i></a>
<?php } ?>
        </td>
    </tr>
<?php
$cnt++;
 } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--- end row -->                                 
                    </div> <!-- container -->
                </div> <!-- content -->
            </div>
        </div>
        <footer class="footer text-right">2018 © Developed by PHPGurukul.</footer>

<?php require_once(__DIR__ . '/inc/scripts.tpl.php');?> 
    </body>
</html>