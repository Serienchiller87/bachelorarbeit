<?php require_once(__DIR__ . '/inc/header.tpl.php');?>
<?php require_once(__DIR__ . '/inc/sidebar.tpl.php');?>
<?php require_once(__DIR__ . '/inc/top.tpl.php');?>
<?php require_once(__DIR__ . '/inc/message.tpl.php');?>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <div class="table-responsive">
                                        <table class="table table-colored table-centered table-inverse m-0">
                                            <thead>
                                                <tr>
                                                    <th>Title</th>
                                                    <th>Category</th>
                                                    <th>Subcategory</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
<?php foreach ($posts as $value) { ?>
    <tr>
        <td><b><?= $value->getPostTitle() ?></b></td>
        <td><?= Category::find($value->getCategoryId())->getCategoryName() ?></td>
        <td><?= Subcategory::find($value->getSubCategoryId())->getSubCatName() ?></td>
        <td>
<?php if($showActivePosts) { ?>
            <a href="index.php?controller=post&action=edit&id=<?= $value->getId() ?>"><i class="fa fa-pencil" style="color: #29b6f6;"></i></a>&nbsp;
            <a href="index.php?controller=post&action=setInactive&id=<?= $value->getId() ?>"> <i class="fa fa-trash-o" style="color: #f05050"></i></a> 
<?php } else { ?>
            <a href="index.php?controller=post&action=restore&id=<?= $value->getId() ?>"> <i class="ion-arrow-return-right" title="Restore this Post"></i></a>&nbsp;
            <a href="index.php?controller=post&action=delete&id=<?= $value->getId() ?>"><i class="fa fa-trash-o" style="color: #f05050" title="Permanently delete this post"></i></a>
<?php } ?>
                                                    </td>
                                                </tr>
<?php } ?>                            
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- container -->
                </div> <!-- content -->
            </div>
        </div>
        <footer class="footer text-right">2018 © Developed by PHPGurukul.</footer>

<?php require_once(__DIR__ . '/inc/scripts.tpl.php');?> 
    </body>
</html>