<?php require_once(__DIR__ . '/inc/header.tpl.php');?>
<?php require_once(__DIR__ . '/inc/sidebar.tpl.php');?>
<?php require_once(__DIR__ . '/inc/top.tpl.php');?>
<?php require_once(__DIR__ . '/inc/message.tpl.php');?>

                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="p-6">
                                    <div class="">
                                        <form name="aboutus" action="index.php?controller=infopage&action=save" method="post">
                                            <div class="form-group m-b-20">
                                                <label for="exampleInputEmail1">Page Title</label>
                                                <input type="text" class="form-control" id="pagetitle" name="pagetitle" value="<?= $page->getPageTitle() ?>"  required>
                                                <input type="hidden" id="pagetype" name="pagetype" value="<?= $page->getPageName() ?>">
                                            </div>      
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="card-box">
                                                        <h4 class="m-b-30 m-t-0 header-title"><b>Page Details</b></h4>
                                                        <textarea class="summernote" name="pagedescription"  required><?= $page->getDescription(); ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="submit" name="update" class="btn btn-success waves-effect waves-light">Update and Post</button>
                                        </form>
                                    </div>
                                </div> <!-- end p-20 -->
                            </div> <!-- end col -->
                        </div>
                        <!-- end row -->
                    </div> <!-- container -->
                </div> <!-- content -->
            </div>
        </div>
        <footer class="footer text-right">2018 © Developed by PHPGurukul.</footer>

<?php require_once(__DIR__ . '/inc/scripts.tpl.php');?> 
<?php require_once(__DIR__ . '/inc/scriptEditorSize.tpl.php');?> 
    </body>
</html>