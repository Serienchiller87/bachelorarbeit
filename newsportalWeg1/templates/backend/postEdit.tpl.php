<?php require_once(__DIR__ . '/inc/header.tpl.php');?>
<?php require_once(__DIR__ . '/inc/sidebar.tpl.php');?>
<?php require_once(__DIR__ . '/inc/top.tpl.php');?>
<?php require_once(__DIR__ . '/inc/message.tpl.php');?>

                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="p-6">
                                    <div class="">
                                        <form name="addpost" action="index.php?controller=post&action=save" method="post" enctype="multipart/form-data">
                                            <div class="form-group m-b-20">
                                                <label for="exampleInputEmail1">Post Title</label>
                                                <input type="text" class="form-control" id="posttitle" value="<?= isset($post) ? $post->getPostTitle() : '' ?>" name="posttitle" placeholder="Enter title" required>
                                                <input id="id" name="id" type="hidden" value="<?= isset($post) ? $post->getId() : '' ?>">
                                            </div>
                                            <div class="form-group m-b-20">
                                                <label for="exampleInputEmail1">Category</label>
                                                <select class="form-control" name="category" id="category" onChange="getSubCat(this.value);" required>
<?php if(isset($post)) { ?>
    <option value="<?= $post->getCategoryId() ?>"><?= Category::find($post->getCategoryId())->getCategoryName() ?></option>
<?php } else { ?>
    <option value="">Select Category </option>
<?php } ?>
<?php foreach ($activeCats as $value) { ?>
    <option value="<?= $value->getId() ?>"><?= $value->getCategoryName() ?></option>
<?php } ?>
                                                </select> 
                                            </div>
                                            <div class="form-group m-b-20">
                                                <label for="exampleInputEmail1">Sub Category</label>
                                                <select class="form-control" name="subcategory" id="subcategory" required>
<?php if(isset($post)) { ?>
    <option value="<?= $post->getSubCategoryId() ?>"><?= Subcategory::find($post->getSubCategoryId())->getSubCatName() ?></option>
<?php } ?>
                                                </select> 
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="card-box">
                                                        <h4 class="m-b-30 m-t-0 header-title"><b>Post Details</b></h4>
                                                        <textarea class="summernote" name="postdescription" required><?= isset($post) ? $post->getPostDetails() : '' ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="card-box">
                                                        <h4 class="m-b-30 m-t-0 header-title"><b>Post Image</b></h4>
<?php if(isset($post)) { ?>
    <img src="postimages/<?= $post->getPostImage() ?>" width="300"/>
    <br />
    <a href="index.php?controller=post&action=image&id=<?= $post->getId() ?>">Update Image</a>
<?php } else { ?>
    <input type="file" class="form-control" id="postimage" name="postimage"  required>
<?php } ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <button type="submit" name="submit" class="btn btn-success waves-effect waves-light">Save and Post</button>
                                            <button type="button" class="btn btn-danger waves-effect waves-light">Discard</button>
                                        </form>
                                    </div>
                                </div> <!-- end p-20 -->
                            </div> <!-- end col -->
                        </div>
                        <!-- end row -->
                    </div> <!-- container -->
                </div> <!-- content -->
            </div>
        </div>
        <footer class="footer text-right">2018 © Developed by PHPGurukul.</footer>
        <script>
            function getSubCat(val) {
                $.ajax({
                    type: "POST",
                    url: "../inc/ajaxSubcategory.php",
                    data:'catid=' + val,
                    success: function(data){
                        var subcats = JSON.parse(data);
                        $('#subcategory').empty().append('<option value="">Select Subcategory</option>');
                        for (i in subcats) {
                            $('#subcategory').append('<option value="' + subcats[i].key + '">' + subcats[i].value + '</option>');
                        }
                    }
                });
            }
        </script>

<?php require_once(__DIR__ . '/inc/scripts.tpl.php');?>        
<?php require_once(__DIR__ . '/inc/scriptEditorSize.tpl.php');?> 
    </body>
</html>