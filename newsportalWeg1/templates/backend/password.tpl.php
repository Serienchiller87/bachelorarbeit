<?php require_once(__DIR__ . '/inc/header.tpl.php');?>
<?php require_once(__DIR__ . '/inc/sidebar.tpl.php');?>
<?php require_once(__DIR__ . '/inc/top.tpl.php');?>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <h4 class="m-t-0 header-title"><b><?= $pageTitle ?> </b></h4>
                                    <hr />
                      
<?php require_once(__DIR__ . '/inc/message.tpl.php');?>

                                    <div class="row">
                                        <div class="col-md-10">
                                            <form class="form-horizontal" name="chngpwd" action="index.php?controller=user&action=changePassword" method="post" onSubmit="return valid();">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Current Password</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" value="" name="password" required>
                                                    </div>
                                                </div>                                  
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">New Password</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" value="" name="newpassword" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Confirm Password</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" value="" name="confirmpassword" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">&nbsp;</label>
                                                    <div class="col-md-8">
                                                        <button type="submit" class="btn btn-custom waves-effect waves-light btn-md" name="submit">
                                                    Submit</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
                    </div> <!-- container -->
                </div> <!-- content -->
            </div>
        </div>
        <footer class="footer text-right">2018 © Developed by PHPGurukul.</footer>

<?php require_once(__DIR__ . '/inc/scripts.tpl.php');?> 
<?php require_once(__DIR__ . '/inc/scriptValidate.tpl.php');?>       
    </body>
</html>