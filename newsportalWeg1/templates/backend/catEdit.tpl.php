<?php require_once(__DIR__ . '/inc/header.tpl.php');?>
<?php require_once(__DIR__ . '/inc/sidebar.tpl.php');?>
<?php require_once(__DIR__ . '/inc/top.tpl.php');?>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <h4 class="m-t-0 header-title"><b><?= $pageTitle ?> </b></h4>
                                    <hr />          

<?php require_once(__DIR__ . '/inc/message.tpl.php');?>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <form class="form-horizontal" name="category" action="index.php?controller=category&action=save" method="post">
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Category</label>
                                                    <div class="col-md-10">
                                                        <input type="text" class="form-control" value="<?= isset($cat) ? $cat->getCategoryName() : '' ?>" name="category" required>
                                                        <input id="id" name="id" type="hidden" value="<?= isset($cat) ? $cat->getId() : '' ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Category Description</label>
                                                    <div class="col-md-10">
                                                        <textarea class="form-control" rows="5" name="description" required><?= isset($cat) ? $cat->getDescription() : '' ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">&nbsp;</label>
                                                    <div class="col-md-10">
                                                        <button type="submit" class="btn btn-custom waves-effect waves-light btn-md" name="submit">Submit</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
                    </div> <!-- container -->
                </div> <!-- content -->
            </div>
        </div>
        <footer class="footer text-right">2018 © Developed by PHPGurukul.</footer>

<?php require_once(__DIR__ . '/inc/scripts.tpl.php');?> 

    </body>
</html>
