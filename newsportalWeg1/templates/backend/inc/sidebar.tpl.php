<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul>
                <li class="menu-title">Navigation</li>
                <li class="has_sub">
                    <a href="index.php?controller=user&action=index" class="waves-effect"><i class="mdi mdi-view-dashboard"></i> <span> Dashboard </span> </a>   
                </li>     
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-format-list-bulleted"></i> <span> Category </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="index.php?controller=category&action=add">Add Category</a></li>
                        <li><a href="index.php?controller=category&action=index">Manage Category</a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-format-list-bulleted"></i> <span>Sub Category </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="index.php?controller=subcategory&action=add">Add Sub Category</a></li>
                        <li><a href="index.php?controller=subcategory&action=index">Manage Sub Category</a></li>
                    </ul>
                </li>               
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-format-list-bulleted"></i> <span> Posts </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="index.php?controller=post&action=add">Add Posts</a></li>
                        <li><a href="index.php?controller=post&action=indexActive">Manage Posts</a></li>
                         <li><a href="index.php?controller=post&action=indexInactive">Trash Posts</a></li>
                    </ul>
                </li>  
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-format-list-bulleted"></i> <span> Pages </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="index.php?controller=infopage&action=aboutus">About us</a></li>
                        <li><a href="index.php?controller=infopage&action=contactus">Contact us</a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-format-list-bulleted"></i> <span> Comments </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                      <li><a href="index.php?controller=comment&action=indexInactive">Waiting for Approval </a></li>
                        <li><a href="index.php?controller=comment&action=indexActive">Approved Comments</a></li>
                    </ul>
                </li>   
            </ul>
        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>
        <div class="help-box">
            <h5 class="text-muted m-t-0">Need Help?</h5>
            <p class=""><span class="text-custom">Email:</span> <br/> <a href="mailto:randykilian@gmail.com">randykilian@gmail.com</a></p>
        </div>
    </div>
    <!-- Sidebar -left -->
</div>
