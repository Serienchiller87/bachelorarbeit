<!---Success Message--->  
<?php if(isset($_SESSION['msg']) && $_SESSION['msg'] !== ''){ ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-<?= strpos($_SESSION['msg'], 'Error') !== false ? 'danger' : 'success' ?>" role="alert">
                <?= $_SESSION['msg'] ?>
            </div>
        </div>
    </div>
<?php 
    $_SESSION['msg'] = '';
} ?>
