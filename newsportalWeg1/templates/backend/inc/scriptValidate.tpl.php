<script type="text/javascript">
    function valid() {
        if(document.chngpwd.password.value=="") {
            alert("Current password field is empty!");
            document.chngpwd.password.focus();
            return false;
        } else if(document.chngpwd.newpassword.value=="") {
            alert("New Password field is empty!");
            document.chngpwd.newpassword.focus();
            return false;
        } else if(document.chngpwd.confirmpassword.value=="") {
            alert("Confirm password field is empty!");
            document.chngpwd.confirmpassword.focus();
            return false;
        } else if(document.chngpwd.newpassword.value!= document.chngpwd.confirmpassword.value) {
            alert("Password and Confirm password field do not match!");
            document.chngpwd.confirmpassword.focus();
            return false;
        }
        return true;
    }
</script>