<?php require_once(__DIR__ . '/inc/header.tpl.php');?>
<?php require_once(__DIR__ . '/inc/sidebar.tpl.php');?>
<?php require_once(__DIR__ . '/inc/top.tpl.php');?>

                        <div class="row">
                            <a href="index.php?controller=category&action=index">
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="card-box widget-box-one">
                                        <i class="mdi mdi-chart-areaspline widget-one-icon"></i>
                                        <div class="wigdet-one-content">
                                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">Categories Listed</p>
                                            <h2><?= $countCat ?> <small></small></h2>
                                        </div>
                                    </div>
                                </div>
                            </a><!-- end col -->
                            <a href="index.php?controller=subcategory&action=index">
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="card-box widget-box-one">
                                        <i class="mdi mdi-layers widget-one-icon"></i>
                                        <div class="wigdet-one-content">
                                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="User This Month">Listed Subcategories</p>
                                            <h2><?= $countSubcat ?> <small></small></h2>
                                  
                                        </div>
                                    </div>
                                </div><!-- end col -->
                            </a>
                            <a href="index.php?controller=post&action=indexActive">                       
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="card-box widget-box-one">
                                        <i class="mdi mdi-layers widget-one-icon"></i>
                                        <div class="wigdet-one-content">
                                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="User This Month">Live News</p>
                                            <h2><?= $countPosts ?> <small></small></h2>
                                  
                                        </div>
                                    </div>
                                </div><!-- end col -->
                            </a>
                        </div>
                        <!-- end row -->
                        <div class="row">
                            <a href="index.php?controller=post&action=indexInactive"> 
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="card-box widget-box-one">
                                        <i class="mdi mdi-layers widget-one-icon"></i>
                                        <div class="wigdet-one-content">
                                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="User This Month">Trash News</p>
                                            <h2><?= $countTrash ?> <small></small></h2>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div> <!-- container -->
                </div> <!-- content -->
            </div>
        </div>
        <footer class="footer text-right">2018 © Developed by PHPGurukul.</footer>

<?php require_once(__DIR__ . '/inc/scripts.tpl.php');?> 
    </body>
</html>