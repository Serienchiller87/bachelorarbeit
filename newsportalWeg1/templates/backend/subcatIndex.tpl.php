<?php require_once(__DIR__ . '/inc/header.tpl.php');?>
<?php require_once(__DIR__ . '/inc/sidebar.tpl.php');?>
<?php require_once(__DIR__ . '/inc/top.tpl.php');?>
<?php require_once(__DIR__ . '/inc/message.tpl.php');?>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="demo-box m-t-20">
                                    <div class="m-b-30">
                                        <a href="index.php?controller=subcategory&action=add">
                                            <button id="addToTable" class="btn btn-success waves-effect waves-light">Add <i class="mdi mdi-plus-circle-outline" ></i></button>
                                        </a>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table m-0 table-colored-bordered table-bordered-primary">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th> Category</th>
                                                    <th>Sub Category</th>
                                                    <th>Description</th>
                                                    <th>Posting Date</th>
                                                    <th>Last updation Date</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
<?php 
$cnt=1;
foreach ($activeSubcats as $value) { ?>
    <tr>
        <th scope="row"><?php echo $cnt;?></th>
        <td><?= Category::find($value->getCategoryId())->getCategoryName() ?></td>
        <td><?= $value->getSubCatName() ?></td>
        <td><?= $value->getSubCatDescription() ?></td>
        <td><?= $value->getPostingDate() ?></td>
        <td><?= $value->getUpdationDate() ?></td>
        <td><a href="index.php?controller=subcategory&action=edit&id=<?= $value->getId() ?>"><i class="fa fa-pencil" style="color: #29b6f6;"></i></a> 
            &nbsp;<a href="index.php?controller=subcategory&action=setInactive&id=<?= $value->getId() ?>"> <i class="fa fa-trash-o" style="color: #f05050"></i></a> </td>
    </tr>
<?php
$cnt++;
 } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--- end row -->                    
                        <div class="row">
                            <div class="col-md-12">
                                <div class="demo-box m-t-20">
                                    <div class="m-b-30">
                                        <h4><i class="fa fa-trash-o" ></i> Deleted SubCategories</h4>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table m-0 table-colored-bordered table-bordered-danger">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th> Category</th>
                                                    <th>Sub Category</th>
                                                    <th>Description</th>
                                                    <th>Posting Date</th>
                                                    <th>Last updation Date</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
<?php 
$cnt=1;
foreach ($inactiveSubcats as $value) { ?>
    <tr>
        <th scope="row"><?php echo $cnt;?></th>
        <td><?= Category::find($value->getCategoryId())->getCategoryName() ?></td>
        <td><?= $value->getSubCatName() ?></td>
        <td><?= $value->getSubCatDescription() ?></td>
        <td><?= $value->getPostingDate() ?></td>
        <td><?= $value->getUpdationDate() ?></td>
        <td><a href="index.php?controller=subcategory&action=restore&id=<?= $value->getId() ?>"><i class="ion-arrow-return-right" title="Restore this Subcategory"></i></a>&nbsp;<a href="index.php?controller=subcategory&action=delete&id=<?= $value->getId() ?>"> <i class="fa fa-trash-o" style="color: #f05050"></i></a></td>
    </tr>
<?php
$cnt++;
 } ?>
                                            </tbody>     
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>                  
                    </div> <!-- container -->
                </div> <!-- content -->
            </div>
        </div>
        <footer class="footer text-right">2018 © Developed by PHPGurukul.</footer>
<?php require_once(__DIR__ . '/inc/scripts.tpl.php');?> 

    </body>
</html>