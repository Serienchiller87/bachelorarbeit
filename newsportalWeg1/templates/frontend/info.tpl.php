<?php require_once(__DIR__ . '/inc/header.tpl.php');?>
    <!-- Page Content -->
    <div class="container" style="flex: 1;">
        <h1 class="mt-4 mb-3"><?= $page->getPageTitle() ?>
        </h1>

        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.php">Home</a>
            </li>
            <li class="breadcrumb-item active"><?= $pageTitle ?></li>
        </ol>

        <!-- Intro Content -->
        <div class="row">
            <div class="col-lg-12">
                <p><?= $page->getDescription() ?></p>
            </div>
        </div>
        <!-- /.row -->
    </div>

<?php require_once(__DIR__ . '/inc/footer.tpl.php');?>
