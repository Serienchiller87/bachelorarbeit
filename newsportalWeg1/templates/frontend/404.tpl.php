<?php require_once(__DIR__ . '/inc/header.tpl.php');?>
    <!-- Page Content -->
    <div class="container" style="flex: 1;">
        <div class="row" style="margin-top: 4%">
            <!-- Blog Entries Column -->
            <div class="col-md-8">
<?php require_once('templates/backend/inc/message.tpl.php');?>
                <!-- Blog Post -->
                <div class="card mb-4">
                    <div class="card-body">
                        <h2 class="card-title"><?= $pageTitle ?></h2>
                        <p>
                            The requested object was not found on the server. Please try again.
                        </p>         
                    </div>
                    <div class="card-footer text-muted">                
                    </div>
                </div>     
            </div>

            <!-- Sidebar Widgets Column -->
<?php require_once(__DIR__ . '/inc/sidebar.tpl.php');?>
        </div>
        <!-- /.row -->
<?php require_once(__DIR__ . '/inc/comments.tpl.php');?>
    </div>
    <!-- /.container -->
<?php require_once(__DIR__ . '/inc/footer.tpl.php');?>
