<!--- Insert Comment Section --->
<div class="row" style="margin-top: -8%">
    <div class="col-md-8">
        <div class="card my-4">
            <h5 class="card-header">Leave a Comment:</h5>
            <div class="card-body">
                <form name="Comment" action="<?= $url ?>" method="post">
                    <input type="hidden" name="csrftoken" value="<?php echo htmlentities($_SESSION['token']); ?>" />
                    <div class="form-group">
                        <input type="text" name="name" class="form-control" placeholder="Enter your fullname" required>
                    </div>

                    <div class="form-group">
                        <input type="email" name="email" class="form-control" placeholder="Enter your Valid email" required>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="comment" rows="3" placeholder="Comment" required></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                </form>
            </div>
        </div>

        <!---Comment Display Section --->
<?php foreach ($comments as $value) { ?>
        <div class="media mb-4">
            <img class="d-flex mr-3 rounded-circle" src="images/usericon.png" alt="">
            <div class="media-body">
                <h5 class="mt-0"><?= $value->getName() ?> <br />
                    <span style="font-size:11px;"><b>at</b> <?= $value->getPostingDate() ?></span>
                </h5>
         
<?= $value->getComment() ?>            
            </div>
        </div>
<?php } ?>
    </div>
</div>
