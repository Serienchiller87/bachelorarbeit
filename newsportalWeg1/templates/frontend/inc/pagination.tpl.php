<!-- Pagination -->
<ul class="pagination justify-content-center mb-4">
    <li class="page-item"><a href="<?= $url ?>&pageno=1"  class="page-link">First</a></li>
    <li class="<?php if($this->pageNo <= 1){ echo 'disabled'; } ?> page-item">
        <a href="<?php if($this->pageNo <= 1){ echo '#'; } else { echo $url . "&pageno=".($this->pageNo - 1); } ?>" class="page-link">Prev</a>
    </li>
    <li class="<?php if($this->pageNo >= $totalPages){ echo 'disabled'; } ?> page-item">
        <a href="<?php if($this->pageNo >= $totalPages){ echo '#'; } else { echo $url . "&pageno=".($this->pageNo + 1); } ?> " class="page-link">Next</a>
    </li>
    <li class="page-item"><a href="<?= $url ?>&pageno=<?php echo $totalPages; ?>" class="page-link">Last</a></li>
</ul>
