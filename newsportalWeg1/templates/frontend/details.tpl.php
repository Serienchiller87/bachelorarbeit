<?php require_once(__DIR__ . '/inc/header.tpl.php');?>
    <!-- Page Content -->
    <div class="container" style="flex: 1;">
        <div class="row" style="margin-top: 4%">
            <!-- Blog Entries Column -->
            <div class="col-md-8">
<?php require_once('templates/backend/inc/message.tpl.php');?>
                <!-- Blog Post -->
                <div class="card mb-4">
                    <div class="card-body">
                        <h2 class="card-title"><?= $post->getPostTitle() ?></h2>
                        <p><b>Category : </b> <a href="index.php?controller=index&action=category&catid=<?= $post->getCategoryId() ?>"><?= Category::find($post->getCategoryId())->getCategoryName() ?></a> |
                            <b>Sub Category : </b><?= Subcategory::find($post->getSubCategoryId())->getSubCatName() ?> <b> Posted on </b><?= $post->getPostingDate() ?>
                        </p>
                        <hr />

                        <img class="img-fluid rounded" src="admin/postimages/<?= $post->getPostImage() ?>" alt="<?= $post->getPostTitle() ?>"> 
                        <p class="card-text"><?= substr($post->getPostDetails(), 0) ?></p>             
                    </div>
                    <div class="card-footer text-muted">                
                    </div>
                </div>     
            </div>

            <!-- Sidebar Widgets Column -->
<?php require_once(__DIR__ . '/inc/sidebar.tpl.php');?>
        </div>
        <!-- /.row -->
<?php require_once(__DIR__ . '/inc/comments.tpl.php');?>
    </div>
    <!-- /.container -->
<?php require_once(__DIR__ . '/inc/footer.tpl.php');?>
