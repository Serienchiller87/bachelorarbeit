<?php require_once(__DIR__ . '/inc/header.tpl.php');?>
    <!-- Page Content -->
    <div class="container" style="flex: 1;">
        <div class="row" style="margin-top: 4%">
            <!-- Blog Entries Column -->
            <div class="col-md-8">
                <!-- Blog Post -->
<?php 
if(isset($categoryName)) {
    echo '<h1>' . htmlentities($categoryName) .' News</h1><br>';
}
if(count($posts) < 1) {
    echo 'No record found...'; 
} 
foreach ($posts as $value) {
?>
                <div class="card mb-4">
                    <img class="card-img-top" src="admin/postimages/<?= $value->getPostImage() ?>" alt="<?= $value->getPostTitle() ?>">
                    <div class="card-body">
                        <h2 class="card-title"><?= $value->getPostTitle() ?></h2>
                             <p><b>Category : </b> <a href="index.php?controller=index&action=category&catid=<?= $value->getCategoryId() ?>"><?= Category::find($value->getCategoryId())->getCategoryName() ?></a> </p>
                        <a href="index.php?controller=details&action=show&nid=<?= $value->getId() ?>" class="btn btn-primary">Read More &rarr;</a>
                    </div>
                    <div class="card-footer text-muted">
                        Posted on <?= $value->getPostingDate() ?>          
                    </div>
                </div>
<?php } ?>
<?php require_once(__DIR__ . '/inc/pagination.tpl.php');?>    
            </div>

            <!-- Sidebar Widgets Column -->
<?php require_once(__DIR__ . '/inc/sidebar.tpl.php');?>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->

    <!-- Footer -->
<?php require_once(__DIR__ . '/inc/footer.tpl.php');?>
