<?php
declare(strict_types=1);
require_once('inc/init.php');

// Werte aus $_GET holen
$controllerName = ucfirst($_GET['controller'] ?? 'index') . 'Controller';
$action = $_GET['action'] ?? 'index';

// Controller zuweisen & Action ausführen
$controller = new $controllerName();
$controller->$action();
?>
