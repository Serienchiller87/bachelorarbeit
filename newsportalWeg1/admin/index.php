<?php
declare(strict_types=1);
require_once('../inc/init.php');

// Login Status prüfen
if(!isset($_SESSION['login']) && !isset($_POST['username'])) {
    $controllerName = 'UserController';
    $action = 'loginForm';
} else {
    // Werte aus $_GET holen
    $controllerName = ucfirst($_GET['controller'] ?? 'user') . 'Controller';
    $action = $_GET['action'] ?? 'index';   
}

// Controller zuweisen & Action ausführen
$controller = new $controllerName();
$controller->$action();
?>
