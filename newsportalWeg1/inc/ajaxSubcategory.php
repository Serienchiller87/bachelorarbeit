<?php // Ajax Aufruf um dynamisch die Subkategorien nachzuladen
declare(strict_types=1);
require_once('init.php');

if(!empty($_REQUEST["catid"])) {
    $result = array();
    $subcats = Subcategory::findAllActiveByCat(intval($_REQUEST["catid"]));
    foreach ($subcats as $value) { // JSON Objekte zusammenbauen
        $item = array();
        $item['key'] = $value->getId();
        $item['value'] = $value->getSubCatName();
        $result[] = $item;
    }
    echo json_encode($result, JSON_FORCE_OBJECT);
}
?>
