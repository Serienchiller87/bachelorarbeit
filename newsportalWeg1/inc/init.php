<?php
declare(strict_types=1);
define('DB_SERVER','localhost');
define('DB_USER','root');
define('DB_PASS' ,'');
define('DB_NAME','newsportal75');
// Dirs to include in Controllers
define('FRONTDIR', 'templates/frontend/');
define('BACKDIR', '../templates/backend/');
define('RECORDSPERPAGE', 8);

// Initialisierung der Anwendung ////////////////

startSession();
spl_autoload_register('autoloadModels');
spl_autoload_register('autoloadControllers');

/////////////////////////////////////////////////

function startSession() {
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
}

function getPDO(): PDO {
    // DB Verbindung herstellen
    try {
        $options = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        $result = new PDO('mysql:host=' . DB_SERVER . ';dbname=' . DB_NAME, DB_USER, DB_PASS, $options);
    } catch (PDOException $e) {
        throw new PDOException($e->getMessage(), (int)$e->getCode());
    }
    return $result;
}

function autoloadModels(string $aName) {
    // je nach aufrufendem Ordner
    $modelFile = __DIR__ . '/../src/models/' . $aName . '.php';
    if(file_exists($modelFile)) {
        require_once $modelFile;
        $aName::connect(getPDO()); // DB Verbindung herstellen
    }
}

function autoloadControllers(string $aName) {
    // je nach aufrufendem Ordner
    $controllerFile = __DIR__ . '/../src/controllers/' . $aName . '.php';
    if(file_exists($controllerFile)) {
        require_once $controllerFile;
    }
}
